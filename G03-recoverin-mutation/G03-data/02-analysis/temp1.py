# run with exec(open("temp1.py").read()) after python -i handle_pca
base_cmap='jet'
def colors(N,base_cmap=base_cmap):
    if N>1:
        intervals = np.arange(0,1+1/(N-1),1/(N-1))
    else:
        intervals = [0.5]
    return [plt.get_cmap(base_cmap)(i) for i in intervals]
import seaborn as sns
import matplotlib.gridspec as gs
gs=gs.GridSpec(1,2, width_ratios=[8,2])
def do(x,y,nfig,t1,t2):
    
    plt.close(nfig)
    plt.figure(nfig)
    plt.cla()
    c=colors(len(x))
    a1=plt.subplot(gs[0])
    a2=plt.subplot(gs[1])
    for i in range(len(x)):
        a1.plot(x[i],y[i],'.',color=c[i])
        if i%100==0:
            print(f'done {i}/{len(x)}')
    a1.grid()
    a1.set_xlabel(f'component {t1}',size=15)
    a1.set_ylabel(f'component {t2}',size=15)
    a1.tick_params(labelsize=14)
    a1.set_title(f'Wild-type protein PCA: component {t1} vs component {t2}',size=16)
    img = a2.imshow([[0,0]],cmap=base_cmap,vmin=0,vmax=300)
    plt.gca().set_visible(False)
    cb=plt.colorbar(img,ax=a2,ticks=[i for i in range(0,400,100)])
    cb.ax.tick_params(labelsize=14)
    cb.ax.set_yticklabels([f'{i} ns' for i in range(0,400,100)])
    plt.subplots_adjust(top=0.9,bottom=0.12,left=0.15,right=0.85,wspace=0)

sel=range(0,30000,1)
x=np.array(pca_traj[0])[sel]
y=np.array(pca_traj[1])[sel]
z=np.array(pca_traj[2])[sel]

plt.close(10)
plt.figure(10)
plt.cla()
c=colors(len(x))
a1=plt.subplot(gs[0],projection='3d')
a2=plt.subplot(gs[1])

for i in range(len(x)):
    a1.plot([x[i]],[y[i]],[z[i]],'.',color=c[i])
    if i%100==0:
        print(f'done {i}/{len(x)}')
a1.grid()
a1.set_xlabel(f'component 1',size=13)
a1.set_ylabel(f'component 2',size=13)
a1.set_zlabel(f'component 3',size=13)
a1.tick_params(labelsize=11)
a1.set_title(f'Wild-type protein PCA: components 1 vs 2 vs 3',size=16)
img = a2.imshow([[0,0]],cmap=base_cmap,vmin=0,vmax=300)
plt.gca().set_visible(False)
cb=plt.colorbar(img,ax=a2,ticks=[i for i in range(0,400,100)])
cb.ax.tick_params(labelsize=14)
cb.ax.set_yticklabels([f'{i} ns' for i in range(0,400,100)])
plt.subplots_adjust(top=0.9,bottom=0.12,left=0.15,right=0.85,wspace=0)



do(x,y,11,'1','2')
do(x,z,12,'1','3')
do(y,z,13,'2','3')
plt.draw()
plt.pause(0.001)


