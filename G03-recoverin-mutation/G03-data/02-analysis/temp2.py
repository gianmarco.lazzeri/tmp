# run with exec(open("temp2.py").read()) after python -i handle_pca


plt.figure(1)
plt.title('Mutated protein RMSD, 1st frame as reference',size=14)
plt.ylabel('RMSD',size=14)
plt.xlabel('time [ns]',size=14)
plt.tick_params(labelsize=14)
plt.xticks([i*100 for i in range(0,350,50)],[f'{i}' for i in range(0,350,50)])

plt.subplots_adjust(top=0.9,bottom=0.12,left=0.15,right=0.85,wspace=0)

plt.figure(2)
plt.title('Mutated protein, mean contribution to RMSD$^2$, 1st frame as reference',size=14)
plt.ylabel('RMSD$^2$',size=14)
plt.xlabel('residue',size=14)
plt.legend(fontsize=13,loc='upper center',ncol=7)
plt.tick_params(labelsize=14)

plt.subplots_adjust(top=0.9,bottom=0.12,left=0.15,right=0.85,wspace=0)
plt.draw()
plt.pause(0.01)


