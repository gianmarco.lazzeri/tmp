chosen: 0.5 persistence thres (only hb contacts had a clear flexus around there)
interaction <Residue ASN, 20> - <Residue ASN, 99> found in wild type matrix and not in mutated
interaction <Residue ARG, 43> - <Residue GLU, 48> found in wild type matrix and not in mutated
interaction <Residue GLN, 47> - <Residue GLN, 50> found in wild type matrix and not in mutated
interaction <Residue GLN, 50> - <Residue GLN, 67> found in wild type matrix and not in mutated
interaction <Residue SER, 54> - <Residue PHE, 57> found in wild type matrix and not in mutated
interaction <Residue PHE, 56> - <Residue PHE, 57> found in wild type matrix and not in mutated
interaction <Residue TYR, 65> - <Residue PHE, 188> found in wild type matrix and not in mutated
interaction <Residue HSE, 68> - <Residue GLU, 124> found in wild type matrix and not in mutated
interaction <Residue ALA, 75> - <Residue ILE, 88> found in wild type matrix and not in mutated
interaction <Residue ALA, 105> - <Residue ILE, 186> found in wild type matrix and not in mutated
interaction <Residue LEU, 108> - <Residue PHE, 188> found in wild type matrix and not in mutated
interaction <Residue ILE, 129> - <Residue VAL, 193> found in wild type matrix and not in mutated
interaction <Residue LYS, 131> - <Residue DGPS, 226> found in wild type matrix and not in mutated
interaction <Residue LYS, 131> - <Residue DGPS, 234> found in wild type matrix and not in mutated
interaction <Residue MET, 132> - <Residue ILE, 133> found in wild type matrix and not in mutated
interaction <Residue MET, 132> - <Residue PRO, 190> found in wild type matrix and not in mutated
interaction <Residue THR, 138> - <Residue ASN, 146> found in wild type matrix and not in mutated
interaction <Residue TRP, 156> - <Residue GLY, 160> found in wild type matrix and not in mutated
interaction <Residue THR, 176> - <Residue ASN, 179> found in wild type matrix and not in mutated
interaction <Residue ASN, 179> - <Residue ILE, 182> found in wild type matrix and not in mutated
interaction <Residue ASN, 179> - <Residue LEU, 183> found in wild type matrix and not in mutated
interaction <Residue GLU, 181> - <Residue LYS, 196> found in wild type matrix and not in mutated
interaction <Residue LEU, 183> - <Residue GLN, 187> found in wild type matrix and not in mutated
interaction <Residue ARG, 184> - <Residue GLU, 195> found in wild type matrix and not in mutated
interaction <Residue ARG, 184> - <Residue GLU, 199> found in wild type matrix and not in mutated
25 differences
alternative subset of rec_r_mut trajectory (20000,30000,10)
interaction <Residue GLY, 7> - <Residue TYR, 32> found in system matrix and not in other
interaction <Residue ASN, 20> - <Residue ASN, 99> found in system matrix and not in other
interaction <Residue GLN, 47> - <Residue GLN, 50> found in system matrix and not in other
interaction <Residue GLN, 50> - <Residue GLN, 67> found in system matrix and not in other
interaction <Residue SER, 54> - <Residue PHE, 57> found in system matrix and not in other
interaction <Residue PHE, 56> - <Residue PHE, 57> found in system matrix and not in other
interaction <Residue TYR, 65> - <Residue PHE, 188> found in system matrix and not in other
interaction <Residue HSE, 68> - <Residue GLU, 124> found in system matrix and not in other
interaction <Residue ALA, 105> - <Residue ILE, 186> found in system matrix and not in other
interaction <Residue LEU, 108> - <Residue PHE, 188> found in system matrix and not in other
interaction <Residue ASP, 110> - <Residue CAL, 302> found in system matrix and not in other
interaction <Residue ASP, 112> - <Residue CAL, 302> found in system matrix and not in other
interaction <Residue GLU, 121> - <Residue CAL, 302> found in system matrix and not in other
interaction <Residue ILE, 129> - <Residue VAL, 193> found in system matrix and not in other
interaction <Residue LYS, 131> - <Residue DGPS, 226> found in system matrix and not in other
interaction <Residue MET, 132> - <Residue ILE, 133> found in system matrix and not in other
interaction <Residue MET, 132> - <Residue PRO, 190> found in system matrix and not in other
interaction <Residue GLU, 136> - <Residue HSE, 140> found in system matrix and not in other
interaction <Residue THR, 138> - <Residue ASN, 146> found in system matrix and not in other
interaction <Residue TRP, 156> - <Residue GLY, 160> found in system matrix and not in other
interaction <Residue THR, 176> - <Residue ASN, 179> found in system matrix and not in other
interaction <Residue ASN, 179> - <Residue ILE, 182> found in system matrix and not in other
interaction <Residue ASN, 179> - <Residue LEU, 183> found in system matrix and not in other
interaction <Residue GLU, 181> - <Residue LEU, 185> found in system matrix and not in other
interaction <Residue GLU, 181> - <Residue LYS, 196> found in system matrix and not in other
interaction <Residue LEU, 183> - <Residue GLN, 187> found in system matrix and not in other
interaction <Residue ARG, 184> - <Residue GLU, 195> found in system matrix and not in other
interaction <Residue ARG, 184> - <Residue GLU, 199> found in system matrix and not in other
interaction <Residue GLU, 189> - <Residue LYS, 192> found in system matrix and not in other
interaction <Residue GLU, 195> - <Residue GLU, 199> found in system matrix and not in other
30 differences
