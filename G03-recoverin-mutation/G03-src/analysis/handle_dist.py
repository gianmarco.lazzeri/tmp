# dist.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i mdist.py <mol> -d <dcd> -i <input> -o <output> --start <start> --stop <stop> --step <step> -x -y -z --xy -f -c
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -x -> just for the plot title: projection on the x axis
# -t -> just for the plot title: projection on the y axis
# -z -> just for the plot title: projection on the z axis
# --xy -> just for the plot title: projection on the xy plane
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# EXAMPLE
# python -i dist.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein --step 5
# TASK
# computes the distance between each couple of selections (center of mass)
# if just a single selection is specified: nothing happens
# plots results

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md distance analysis tool....')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
from MDAnalysis.analysis import align
from MDAnalysis.analysis.rms import rmsd

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# load file
flag = 1
def check(filename):
    global flag
    if os.path.isfile(filename)==False:
        if args.force:
            print(f'warning! {filename} does not exists')
            return 0
        flag = 0
        return(check(input(f'{filename} does not exists!\n  type filename: ')))
    if args.choose and flag:
        choice = input(f'suggested filename: {filename} - file exists\n  want to load? ')
        flag = 0
        if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
            flag = 1
            return filename
        return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='computes the distance between each couple of selections (center of mass)\nif just a single selection is specified: nothing happens\nplots results')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='useless; here just to match pca.py')
parser.add_argument("-i","--input",default='',help='input folder; just for retrieving file names')
parser.add_argument("-o","--output",default='',help='output path; just for retrieving file names')
parser.add_argument("-s","--selection",nargs='+',help="(just for plot titles)\nis a list of 'selections'\niterate over all possible couples, needs at least two names")
parser.add_argument("-x","--x_axis",action='store_true',help='just for the plot title: project everything on the x axis')
parser.add_argument("-y","--y_axis",action='store_true',help='just for the plot title: project everything on the y axis')
parser.add_argument("-z","--z_axis",action='store_true',help='just for the plot title: project everything on the z axis')
parser.add_argument("--xy",action='store_true',help='just for the plot title: project everything on the xy plane')
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='useless; here just to match dist.py command line')
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection
L = len(sel)
if L<=1: # at least 2 selections
    print('type at least two selections!')
else:

    print('*****************************')
    print('working directory:  ',folder)
    print('working on molecule:',mol)
    print('with data:          ','; '.join(dcd))
    print('selections:         ','; '.join(sel))
    print('output path:        ',output)
    print('loading data...')

    # start-step-stop
    start = max(0,args.start-1)
    step = args.step

    # MAIN CYCLE

    for l1 in range(L-1):

        sel_name_1 = sel[l1]

        # remove unnecessary spaces
        sel_name_1 = ' '.join([x for x in sel_name_1.split(' ') if x != ''])
        sel_output_1 = output + '_' + '_'.join([x for x in sel_name_1.split(' ') if x not in mute_words])[0:N_sel]

        for l2 in range(l1+1,L):

            sel_name_2 = sel[l2]
            # remove unnecessary spaces
            sel_name_2 = ' '.join([x for x in sel_name_2.split(' ') if x != ''])
            sel_output_2 = '_VS' + '_' + '_'.join([x for x in sel_name_2.split(' ') if x not in mute_words])[0:N_sel]

            print('*****************************')  
            print(f'loading distance between selections {sel_name_1}, {sel_name_2}'+args.x_axis*'\n         (projecting on the x axis)'+args.y_axis*'\n         (projecting on the y axis)'+args.z_axis*'\n         (projecting on the z axis)'+args.xy*'\n         (projecting on the xy plane)')

            filename = check(sel_output_1+sel_output_2+'_DIST.csv')

            if filename:
                result = np.loadtxt(filename)

                stop = len(result)
                I = [i for i in range(start,stop*step,step)]

                print('plotting results...')
                n_fig += 1
                plt.figure(n_fig)
                plt.grid()
                plt.title(f'distance between {" ".join([x for x in sel_name_1.split(" ") if x not in mute_words])} and {" ".join([x for x in sel_name_2.split(" ") if x not in mute_words])}\n(center of mass'+(args.x_axis)*', projection on the x axis'+(args.y_axis)*', projection on the y axis'+(args.z_axis)*', projection on the z axis'+(args.xy)*', projection on the xy plane'+')')
                plt.ylabel('distance')
                plt.xlabel('epocs')
                plt.xlim(min(I)-step,max(I)+step)
                plt.plot(I,result)
                print(f'>>> handle with plt.figure({n_fig})')

                print(f'distance is {np.mean(result):.3f} +- {np.std(result):.3f}  ({min(result):.3f}, {max(result):.3f})')
                
    print('*****************************')
    print('done')
