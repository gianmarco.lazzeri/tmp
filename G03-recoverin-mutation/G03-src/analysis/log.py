# log.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i log.py <log_file> -i <input> -o <output>
# -f force, no complain if overwriting
# -c choose each file name time by time, --force wins over --choose!
# --start <n> is the time step you start from
# --step <n> is the output frequency (must be compatible with energyfreq in namd configuration file)
# --stop <n> is the time step you end with (compatibly with step)
# --handle -> don't automatically save figures, do it by yourself
# EXAMPLE
#
# TASK
# read log and extrapolate results

# PARAMETERS

time_step = 2 # picoseconds, in simulations


# BEGIN

print('*****************************')
print('md namd2 log analysis tool...')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
from MDAnalysis.analysis import align
from MDAnalysis.analysis.rms import rmsd
from cycler import cycler
c = plt.get_cmap('tab20b').colors
plt.rcParams['axes.prop_cycle'] = cycler(color=c)

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='for each element in <reference>, for each element in <selection>:\ncomputes "selection" rmsd with respect to reference (time series)\ncomputes contributes to rmsd according to residue (time series)')
# main arguments
parser.add_argument("log",help='is a namd2 .log file')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
#
args   = parser.parse_args()

# log
log = args.log
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=log.split('.log')[0]

print('*****************************')
print('working directory:  ',folder)
print(f'working on log file: {log}')
print('output path:        ',output)
print('loading data...     ')

# start-step-stop
start = max(0,args.start-1)
step = args.step
stop = args.stop

nstep = 0
nframe = 0
md_keys = []
md_data = {}

# universe
with open(log) as f:
    for line in f:
        # define keys and initialize to zero
        if line[0:7] == 'ETITLE:' and len(md_keys)==0:
            md_keys = line.split()[1:len(line.split())]
            for x in md_keys:
                md_data[x]=[]
        # fill keys
        if line[0:7] == 'ENERGY:':
            temp = line.split()
            ts = int(temp[1])
            if nframe:
                while md_data[md_keys[0]][nframe-1] >= ts:
                    nframe -= 1
            if ts % step != 0:
                continue
            if ts < start:
                continue
            for i in range(1,len(temp)):
                md_data[md_keys[i-1]] = md_data[md_keys[i-1]][0:nframe]+[np.float32(temp[i])]
            nframe +=1
            if stop > 0:
                if ts > stop:
                    break

# time array
T = np.array(md_data['TS'])*time_step*1e-6
            

# statistics
print('n frames:',nframe)

# print

filename = output+'_raw_TS.csv'
for key in md_data:

    if key == 'TS':
        continue
    print(f'plotting key: {key}...')
    n_fig += 1
    plt.figure(n_fig,figsize=(10,6))
    plt.grid()
    Y = np.array(md_data[key])
    mean = np.mean(Y)
    plt.plot(T,Y)
    plt.xlim(min(T)-1,max(T)+1)
    plt.xlabel('time (ns)')
    plt.ylabel(key)
    plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
    plt.title(f'{key} with time')
    plt.plot([min(T),max(T)],[mean,mean],dashes=[4,2],label=f'mean = {mean:.3g}',color='r')
    plt.legend()
    if args.handle==False:
        filename = check(filename.split('.csv')[0].split('.png')[0].split('_raw_')[0]+'_raw_'+key+'.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')
    print('saving data to file...')
    filename = check(filename.split('.csv')[0].split('.png')[0].split('_raw_')[0]+'_raw_'+key+'.csv')
    np.savetxt(filename,np.c_[T,Y])
    print(f'>>> {filename}')
            
print('*****************************')
print('done')
