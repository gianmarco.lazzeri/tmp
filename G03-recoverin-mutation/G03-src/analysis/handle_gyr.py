# handle_gyr.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i handle_gyr.py <mol> -d <dcd> -i <input> -o <output> -s <selection> --start <n> --stop <n> --step <n> -f -c
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> useless; here just to match gyr.py command line 
# EXAMPLE
# python -i gyr.py protein.psf -d protein_new.dcd -i 01-raw/01-external -o ../../02-analysis/protein_temp -s "resname ALA" --step 5
# TASK
# for each element in <reference>:
# for each element in <selection>:
# computes "selection" rmsd with respect to reference (time series)
# computes contributes to rmsd according to residue (time series)

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
N_ref='1' # default frame for rmsd
sel_default = 'backbone' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md gyr radius replotting tool')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# load file
flag = 1
def check(filename):
    global flag
    if os.path.isfile(filename)==False:
        if args.force:
            print(f'warning! {filename} does not exists')
            return 0
        flag = 0
        return(check(input(f'{filename} does not exists!\n  type filename: ')))
    if args.choose and flag:
        choice = input(f'suggested filename: {filename} - file exists\n  want to load? ')
        flag = 0
        if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
            flag = 1
            return filename
        return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='for each element in <selection>:\nretrieve gyration radius data and plot data (time series)')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='useless; here just to match gyr.py')
parser.add_argument("-i","--input",default='',help='input folder; just for retrieving file names')
parser.add_argument("-o","--output",default='',help='output path; just for retrieving file names')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"(just for plot titles)\nis a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='useless; here just to match gyr.py command line')
parser.add_argument("--dt",type=float,default=10,help="dt step between each frame (ps), default 10")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('output path:        ',output)
print('loading data...')

# start-step-stop
start = max(0,args.start-1)
step = args.step


# MAIN CYCLE

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])
    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    print('*****************************')
    print('loading gyration radius for selection:',sel_name)
    filename = check(sel_output+'_gyr.csv')

    if filename:
        result = np.loadtxt(filename)
        rmean = [result[i-int(N/2):i+int(N/2)].mean() for i in range(int(N/2),len(result)-int(N/2))]
        stop = len(result)
        I = [i for i in range(start,stop*step,step)]
        J = [i*args.dt/1000 for i in I]
        print('plotting results...')
        n_fig += 1
        plt.figure(n_fig)
        plt.grid()
        plt.plot(J,result)
        plt.xlim(min(J),max(J)+1e-3)
        plt.title(f'Radius of gyration for {" ".join([x for x in sel_name.split(" ") if x not in mute_words])}',size=14)
        plt.ylabel('gyr radius',size=14)
        plt.xlabel('time [ns]',size=14)
        plt.tick_params(labelsize=14)
        plt.xticks([i for i in range(int(min(J)),int(max(J))+50,50)])
        plt.subplots_adjust(top=0.9,bottom=0.12,left=0.15,right=0.85,wspace=0)
        print(f'>>> handle with plt.figure({n_fig})')

print('*****************************')
print('done')
