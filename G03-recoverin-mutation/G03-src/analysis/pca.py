# pca.py
#
# SYNTAX
# (conva activate QCB_traject)
# python -i pca.py <mol> -d <dcd> -i <input> -o <output> -s <selection> --start <n> --stop <n> --step <n> -f -c --handle
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -f force, no complain if overwriting
# -c choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# --segments <n> is the number of segments you want to split pca in
# --handle -> don't automatically save figures, do it by yourself
# EXAMPLE
# python -i pca.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein --step 5
# TASK
# perform pca on dcd file
# plot results (2d/3d plots)
# save main components to file

# PARAMETERS
N_eigen=20 # pca: n of eigenvalues shown in plot
N_comp=3 # n of PCA components to be saved

N_sel=20 # max n of characters for selection in default filenames
sel_default = 'backbone' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md pca analysis tool.........')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
from MDAnalysis.analysis import align
import MDAnalysis.analysis.pca as pca

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('       want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='compute pca on dcd (list) file\nplot results (2d/3d plots)\nsave main components to file')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='is a list of dcd or xtc files: coordinate time series\nif not specified: takes <molecule>.dcd as default')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"is a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--segments",type=int,default=epocs,help='is the number of segments you want to split pca in')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection
#
epocs = args.segments

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('output path:        ',output)
print('loading data...')
print('*****************************')

# universe
universe = mda.Universe(mol,dcd)
ref_universe = mda.Universe(mol)

# statistics
print('   n frames:',universe.trajectory.n_frames)
print('total atoms:',universe.select_atoms("all").n_atoms)
print('    protein:',universe.select_atoms("protein or resname GLYM").n_atoms)
print('   membrane:',universe.select_atoms("resname DGPS DGPC DGPE").n_atoms)
print('    calcium:',universe.select_atoms("resname CAL").n_atoms)
print('      water:',universe.select_atoms("resname TIP3").n_atoms)

# start-step-stop
start = max(0,args.start-1)
step = args.step
stop =(args.stop>=0)*min(args.stop+1,universe.trajectory.n_frames)+(args.stop<0)*universe.trajectory.n_frames
universe.transfer_to_memory(start=start,stop=stop,step=step)

# MAIN CYCLE

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])

    real_sel_name = sel_name
    # necessary with myrystoil groups
    if sel_name == 'backbone':
        real_sel_name = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
    if sel_name == 'protein':
        real_sel_name = "protein or same segid as protein"
    if sel_name == 'membrane':
        real_sel_name = "resname DGPS DGPC DGPE"

    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    print('*****************************')

    print('aligning trajectory to pdb frame... ',end='')
    aligner = align.AlignTraj(universe,ref_universe,select=real_sel_name,weights='mass',in_memory=True).run()
    selection = universe.select_atoms(real_sel_name)
    print('done')

    print('running PCA for selection:',sel_name)
    print('selection has',selection.n_atoms,'atoms and',universe.trajectory.n_frames,'frames')
    pca_data  = pca.PCA(universe,select=real_sel_name,verbose=True).run()
    pca_eigen = pca_data.variance

    print('plotting PCA eigenvalues...')
    n_fig += 1
    plt.grid(zorder=0)
    plt.xlim(0.5,N_eigen+0.5)
    plt.figure(n_fig)
    plt.bar([x for x in range(1,N_eigen+1)],pca_eigen[0:N_eigen],width=1,zorder=3)
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: first {N_eigen} eigenvalues")
    plt.xticks(np.arange(N_eigen)+1)
    plt.ylabel('eigenvalue')
    if args.handle==False:
        filename = check(sel_output+'_pca_eigenvalues.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')
    print('saving results to file...')
    filename = check(filename.split('.png')[0]+'.csv')
    np.savetxt(filename,pca_data.variance)
    print('>>>',filename)    

    print(f'saving first {N_comp} PCA components to csv file')
    pca_space = pca_data.transform(selection,n_components=max(3,N_comp))
    pca_traj = [pca_space[:,i] for i in range(max(3,N_comp))] #proj. on first components
    filename = sel_output+'_pca_component'
    for i in range(N_comp):
        temp = filename.split(str(i))
        filename = check(str(i).join(temp[0:max(1,len(temp)-1)])+str(i+1)+'.csv')
        np.savetxt(filename,pca_traj[i])
        print('>>>',filename)

    print('plotting PCA elements...')

    # split epocs
    split=[x for x in range(0,len(pca_traj[0])+1,int(len(pca_traj[0])/epocs))]
    if len(split) < epocs+1:
        split.append(len(pca_traj[0]))

    n_fig += 1
    plt.figure(n_fig)
    plt.grid()
    a = pca_traj[0]
    b = pca_traj[1]
    [plt.plot(a[split[i]:split[i+1]],b[split[i]:split[i+1]],label='frames '+str(split[i]+1)+'-'+str(split[i+1])) for i in range(epocs)]
    plt.legend()
    plt.ticklabel_format(style='sci',scilimits=(0,0))
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: components 1 vs 2")
    plt.xlabel('component 1')
    plt.ylabel('component 2')
    if args.handle==False:
        filename = check(sel_output+'_pca_12.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

    n_fig += 1
    plt.figure(n_fig)
    plt.grid()
    a = pca_traj[0]
    b = pca_traj[2]
    [plt.plot(a[split[i]:split[i+1]],b[split[i]:split[i+1]],label='frames '+str(step*split[i]+1)+'-'+str(step*split[i+1])) for i in range(epocs)]
    plt.legend()
    plt.ticklabel_format(style='sci',scilimits=(0,0))
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: components 1 vs 3")
    plt.xlabel('component 1')
    plt.ylabel('component 3')
    if args.handle==False:
        filename = check(filename.split('.png')[0].split('12')[0]+'13.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

    n_fig +=1
    plt.figure(n_fig)
    plt.grid()
    a = pca_traj[1]
    b = pca_traj[2]
    [plt.plot(a[split[i]:split[i+1]],b[split[i]:split[i+1]],label='frames '+str(step*split[i]+1)+'-'+str(step*split[i+1])) for i in range(epocs)]
    plt.legend()
    plt.ticklabel_format(style='sci',scilimits=(0,0))
    plt.title(f"{' '.join([x for x in sel_name.split(' ') if x not in mute_words])} pca: components 2 vs 3")
    plt.xlabel('component 2')
    plt.ylabel('component 3')
    if args.handle==False:
        filename = check(filename.split('.png')[0].split('13')[0]+'23.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

    # 3d plot
    n_fig += 1
    fig = plt.figure(n_fig)
    ax = Axes3D(fig)
    [ax.plot(pca_traj[0][split[i]:split[i+1]],pca_traj[1][split[i]:split[i+1]],pca_traj[2][split[i]:split[i+1]],label='frames '+str(split[i]+1)+'-'+str(split[i+1])) for i in range(epocs)]
    ax.set_title(sel_name+" pca: components 1 vs 2 vs 3")
    ax.legend()
    ax.set_xlabel('component 1')
    ax.set_ylabel('component 2')
    ax.set_zlabel('component 3')
    if args.handle==False:
        filename = check(filename.split('.png')[0].split('23')[0]+'123.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

print('*****************************')
print('done')
