        k = [k.lower() for k in k]
        dcdfreq = None
        keys    = None
        data    = None
        ts      = None
        steps   = 0
        J = [] # keys to be kept
        with open((len(input_folder)>0)*f'{input_folder}/'+log) as f:
            for line in f:
                if   not ts      and 'Info: TIMESTEP' in line:
                    ts      = float(line.split()[-1])/1e6 # step in ns
                elif not dcdfreq and 'Info: DCD FREQUENCY' in line:
                    dcdfreq = int(line.split()[-1])
                elif not keys    and line[0:7] == 'ETITLE:':
                    keys    = line.split()[2:]
                    data    = {'T':[]}
                    for j in range(len(keys)):
                        if keys[j].lower() in k or len(k)==0:
                            J.append(j)
                            data[keys[j]] = []
                if keys and dcdfreq and ts:
                    break
            self.dt = dcdfreq*ts*step
            T=[]
            i = 1
            for line in f:
                # fill keys
                if line[0:7] == 'ENERGY:':
                    fields = line.split()
                    if int(fields[1]) != i*dcdfreq*step:
                        continue
                    fields = fields[2:]
                    T.append(i*self.dt)
                    data['T'].append(T[-1])
                    for j in J:
                        data[keys[j]].append(float(fields[j]))
                    i += 1
        fname = fname.split('.pdb')[0] if fname\
                else f'{self.fname(output_name)}{suffix}'
        for key in data:
            if key == 'T':
                continue
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            title = set_optional(args,'title',default_title)
            title += f' {key.lower()}'
            figure()
            results = data[key]
            plt.plot(T,results,'.')
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel(f'{key.lower()}')
            plt.title(title)
            draw()
            pause()
            print(verbose*f'plotted {key.lower()}; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = check_save(fname+f'_{key}.pdf',choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
                fname=fname.split(f'_{key}.pdf')[0].split(f'{key}.pdf')[0].\
                            split('.pdf')[0]
        if output:
            return data
