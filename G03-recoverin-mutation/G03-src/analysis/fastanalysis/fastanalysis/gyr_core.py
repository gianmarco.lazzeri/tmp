        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'computing gyration radius for ',end='')
                print(verbose*f'selection "{selection_name}"\n',end='')
                results = [self.universe_selection.radius_of_gyration()\
                for frame in tqdm(self.universe.trajectory,disable=not verbose)]
                T  = [x*self.dt for x in range(len(results))]
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}.csv',choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            self.dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= ', radius of gyration'
            title = set_optional(args,'title',default_title)
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted gyration radius; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__gyr = results
        if output:
            return T,results
