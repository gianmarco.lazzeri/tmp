    def fname(self,name=None,selection_name=None):
        """
        Provide default file address for loading/saving, with neither suffix
        nor extension.

        Parameters
        ----------
        name : str, default None
            Prefix of the file name. If None: takes "self.output_name".
        selection_name : str, default None
            Builds selection suffix upon "selection_name". If None: takes
            "self.selection_name".
            
        Returns
        -------
        fname : str
            
        See Also
        --------
        system.short : related method
        """
        name  = name if name else self.output_name
        sel   = selection_name if type(selection_name) is str\
                             else self.selection_name
        fname = (self.input_folder!='')*f'{self.input_folder}/'+name
        fname = fname[:-4] if fname[-4:] == '.pdb' or fname[-4:] == '.gro'\
                or fname[:-4] == '.dcd' or fname[:-4] =='.csv' else fname        
        return fname+'_'+short(sel) if short(sel) else fname
