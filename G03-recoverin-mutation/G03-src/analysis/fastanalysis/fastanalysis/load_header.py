    def load(self,save=False,**args):
        """
        Load the "input_name" universe, the "reference_name" universe and the
        corresponding selections. Do nothing if it would change nothing.
        
        Parameters
        ----------
        save : bool, default is False
            If True: save universe selection .pdb and .dcd to file.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start : int, optional
            The frame to start with when loading the "input_name" universe.
        step : int, optional
            The frame interval when loading the "input_name" universe.
        stop : int, optional
            The frame to end at when loading the "input_name" universe.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is False, False.
        verbose : bool, optional
            Default is False.
        """
