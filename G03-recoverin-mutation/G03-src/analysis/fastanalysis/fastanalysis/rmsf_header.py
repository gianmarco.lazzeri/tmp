    def rmsf(self,compute=True,save=True,plot=True,**args):
        """
        Return the rmsf of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute rmsd.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,results,results_residues : tuple
            "T" is the time vector, "results" is the rmsd vector,
            "results_residues" is the "(residue_name,rmsd2 vector)"
            list of tuples.
        """
        # parameters
        fname  = ''
        suffix = '_rmsf'
