        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'computing rmsf for selection ',end='')
                print(verbose*f'"{selection_name}"\n',end='')
                results=RMSF(self.universe_selection,verbose=verbose).run().rmsf
                results_residues = []
                residues = []
                print(verbose*'computing rmsf by residue\n',end='')
                for residue in tqdm(self.universe_selection.residues,
                                disable=not verbose):
                    residues.append(residue.resname)
                    results_residues.append(mean(RMSF(residue.atoms).run().rmsf))
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,results,delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving residues dictionary to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}'+\
                                      '_residues.dic',choose=choose,force=force)
                    savetxt(fname,residues,fmt='%s')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving rmsf by residue data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                      '_residues.csv',choose=choose,force=force)
                    savetxt(fname,results_residues)
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
                else:
                    return
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}.csv',choose=choose,force=force)
            results = loadtxt(fname)
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading residues dictionary\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                              f'{suffix}_residues.dic',choose=choose,force=force)
            with open(fname) as f:
                residues = [res.replace('\n','') for res in f.readlines()]
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading rmsf2 by residue data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                              f'_residues.csv',choose=choose,force=force)
            results_residues = loadtxt(fname)
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= ', RMSF by residue'
            title = set_optional(args,'title',default_title)
            figure(figsize=(8.533,4.8))
            plt.grid(zorder=0)
            plt.xlim(0,len(residues))
            d = max(results_residues)-min(results_residues)
            plt.ylim(max(0,min(results_residues)-d*.1),
                     max(results_residues)+d*.3)
            residues_dictionary={}
            for i,residue in enumerate(residues):
                if residue in residues_dictionary:
                    residues_dictionary[residue].append(i)
                else:
                    residues_dictionary[residue] = [i]
            col = colors(len(residues_dictionary))
            for i,residue in enumerate(residues_dictionary):
                plt.bar([j+1/2 for j in residues_dictionary[residue]],\
          [results_residues[j] for j in residues_dictionary[residue]],\
                        width=1,color=col[i],zorder=4,label=residue)
            plt.legend(loc='upper center',\
                       ncol=int(len(residues_dictionary)/3),fontsize=9)
            plt.xlabel('residues')
            plt.ylabel('RMSF [A]')      
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted rmsf by residue; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__rmsf = results_residues
        if output: 
            return results,\
            [(residues[i],results_residues[i]) for i in range(len(residues))]
