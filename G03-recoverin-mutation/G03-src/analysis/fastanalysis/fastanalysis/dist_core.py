        remove = []
        if 'x' not in axis:
            remove.append(0)
        if 'y' not in axis:
            remove.append(1)
        if 'z' not in axis:
            remove.append(2)
        sel_name  = real_selection_name(sel)
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                if len(remove) == 3:
                    print(verbose*'warning: wrong "axis" argument\n',end='')
                    return [],[]
                print(verbose*'computing distance for ',end='')
                print(verbose*f'selection "{selection_name} ',end='')
                print(verbose*f'vs selection "{sel}"',end='')
                print(verbose*(len(remove)<3 and len(remove)>0)\
                             *f', projection on the {axis} ',end='')
                print(verbose*(len(remove)==1)*'plane\n',end='')
                print(verbose*(len(remove)==2)*'axis\n',end='')
                selection = self.universe.select_atoms(sel_name)
                results = []
                for frame in tqdm(self.universe.trajectory,disable=not verbose):
                    x = self.universe_selection.center_of_mass()
                    y = selection.center_of_mass()
                    x[remove] *= 0
                    y[remove] *= 0
                    results.append(module(x-y))
                T  = [x*self.dt for x in range(len(results))]
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                   f'_{short(sel)}'+\
                                   f'{"_"+axis if len(remove) else ""}.csv',\
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}_{short(sel)}'+\
                            f'{"_"+axis if len(remove) else ""}.csv',
                                                  choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            self.dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= f' vs {short(sel)} distance'
            title = set_optional(args,'title',default_title)
            title += f', proj. on the {axis} plane'\
                     if len(remove)==1 else\
                     f', proj. on the {axis} axis'
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted distance; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}_{short(sel)}'+\
                            f'{"_"+axis if len(remove) else ""}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__dist = results
        if output:
            return T,results
