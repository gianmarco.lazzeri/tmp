    def log(self,log,**args):
        """
        Get info from vmd log.
        """
        fname = ''
        suffix = '_log'
        with open(input_name+'/'+log) as f:
            for line in f:
                # define keys
                if line[0:7] == 'ETITLE:' and len(md_keys)==0:
                    keys = line.split()[1:len(line.split())]
                # fill keys
                if line[0:7] == 'ENERGY:':
                    temp = line.split()
                    ts = int(temp[1])
                    if nframe:
                        while md_data[md_keys[0]][nframe-1] >= ts:
                            nframe -= 1
                    if ts % step != 0:
                        continue
                    if ts < start:
                        continue
                    for i in range(1,len(temp)):
                        md_data[md_keys[i-1]] = md_data[md_keys[i-1]][0:nframe]+[np.float32(temp[i])]
                    nframe +=1
                    if stop > 0:
                        if ts > stop:
                            break

        # time array
        T = np.array(md_data['TS'])*time_step*1e-6
                    

        # statistics
        print('n frames:',nframe)

        filename = output+'_raw_TS.csv'
        for key in md_data:

            if key == 'TS':
                continue
            print(f'plotting key: {key}...')
            n_fig += 1
            plt.figure(n_fig,figsize=(10,6))
            plt.grid()
            Y = np.array(md_data[key])
            mean = np.mean(Y)
            plt.plot(T,Y)
            plt.xlim(min(T)-1,max(T)+1)
            plt.xlabel('time (ns)')
            plt.ylabel(key)
            plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
            plt.title(f'{key} with time')
            plt.plot([min(T),max(T)],[mean,mean],dashes=[4,2],label=f'mean = {mean:.3g}',color='r')
            plt.legend()
            if args.handle==False:
                filename = check(filename.split('.csv')[0].split('.png')[0].split('_raw_')[0]+'_raw_'+key+'.png')
                plt.savefig(filename)
                print('>>>',filename)
            print(f'>>> handle with plt.figure({n_fig})')
            print('saving data to file...')
            filename = check(filename.split('.csv')[0].split('.png')[0].split('_raw_')[0]+'_raw_'+key+'.csv')
            np.savetxt(filename,np.c_[T,Y])
            print(f'>>> {filename}')
