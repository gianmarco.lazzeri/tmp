"""
Fast analysis of md results. Useful with gromacs or namd.
"""

__version__ = 0.1

# packages

import matplotlib.pyplot   as plt
import matplotlib.patches  as patches
from MDAnalysis.analysis.align import AlignTraj
from scipy.optimize        import curve_fit
from matplotlib.gridspec   import GridSpec 
from mpl_toolkits.mplot3d  import Axes3D         
from matplotlib.pyplot     import gcf,figure,savefig,draw
from numpy.linalg import eigvals
from seaborn      import heatmap
from tqdm         import tqdm
from networkx     import Graph,connected_components
from numpy        import loadtxt,savetxt,transpose,mean,linspace,abs,array
from numpy        import arange,zeros,unique,diff,exp,argmax,dot,concatenate
from os.path      import isfile
from MDAnalysis   import Universe,Writer
from MDAnalysis.analysis      import hbonds
from MDAnalysis.analysis.pca  import PCA
from MDAnalysis.analysis.rms  import rmsd,RMSF
from MDAnalysis.analysis.base import analysis_class
from MDAnalysis.lib.distances import capped_distance, self_capped_distance
from MDAnalysis.lib.distances import distance_array, self_distance_array

pause = lambda : plt.pause(0.001)
module = lambda x: x.dot(x)**(1/2) 

# parameters

DEF_N      = 16        # length of selection short name when saving to file
DEF_DT     = 1e-3      # time step between frames in ns
DEF_SEL    = 'all'     # default selection
DEF_REF    = 0         # default reference frame for reference universe
DEF_PCA    = 3         # n of PCA components to be saved
DEF_EIG    = 20        # n of PCA eigenvalues
DEF_VALS   = arange(0,1,1/100) # pyfferaph issues
DEF_CMAP   = 'nipy_spectral' # barplot issues
DEF_CMAP2  = 'jet'     # scatterplot issues
MUTE_WORDS = ['','resname','name','segid','resid','type','moltype','and','or',\
              '(',')','{','}','index','as','all']
                       # don't take into account while saving
ALIAS      = {'protein' :'protein or same segid as protein',
              'membrane':'resname DPGS DGPC DGPE',
              'calcium' :'name CAL',
              'water'   :'resname TIP3',
              'backbone':'(name N HN CA HA C O OT1 OT2 HT2) '+\
                         'and same segid as (resname MET ALA LYS)',
              'basic'   :'(resname LYS and name NZ HZ*) or '+\
                         '(resname ARG and name CZ NH* 1HH* 2HH*) or '+\
                         'resname CAL',
              'acidic'  :'(resname ASP and name CG OD*) or '+\
                         '(resname GLU and name CD OE*) or '+\
                         '(resname DGPS and name C13 O13A O13B)',
              'chain1'  :'name O21 O22 C21 C22 C23 C24 C25 C26 C27 C28 C29 '+\
                       '0C21 1C21 2C21 3C21 4C21 5C21 6C21 7C21 8C21 9C21 0C22',
              'chain2'  :'name O31 O32 C31 C32 C33 C34 C35 C36 C37 C38 C39 '+\
                       '0C31 1C31 2C31 3C31 4C31 5C31 6C31 7C31 8C31 9C31 0C32',
              'hydrophobic':'resname ALA ILE VAL LEU PHE MET TRP PRO GLYM'
             }         # short names / refefinitions for selections

# functions

def set_optional(args,name,default):
    return args.get(name) if type(args.get(name)) is type(default) else default

def check_save(fname,force=False,choose=False):
    """
    Check if fname address already exists before saving.
    
    Parameters
    ----------
    fname : str
        The output address to be checked.
    force : bool, optional
        If true: don't mind if the name already exists; print warning.
        Wins over "choose".
    choose : bool, optional
        If true: ask for confirmation even when fname doesn't exist.
        Looses with "force".
        
    Returns
    -------
    fname : str
        The accepted version of the file address.
    
    See Also
    --------
    check_load : "inverse" function 
    """
    text = ''
    if isfile(fname) and not force:
        text = f'{fname} already exists!\n  want to overwrite? '
    if choose and not(isfile(fname)) and not force:
        text = f'suggested file name: {fname}\n  want to keep? '
    if text: # use text itself as a flag
        choice = input(text).lower()
        if choice == 'yes' or choice == 'y' or choice == '':
            return fname
        return check_save(input('  type new filename: '),choose=False)
    if isfile(fname):
        print(f'warning: {fname} already exists')
    return fname
    
def check_load(fname,force=False,choose=False):
    """
    Check if fname address exists before loading.
    
    Parameters
    ----------
    fname : str
        The output address to be checked.
    force : bool, optional
        If true: don't mind if the name doesn't exists; print warning,
        return None. Wins over "choose".
    choose : bool, optional
        If true: ask for confirmation even when fname exists.
        Looses with "force".
        
    Returns
    -------
    fname : str
        The accepted version of the file address.
    
    See Also
    --------
    check_save : "inverse" function 
    """
    text = ''
    if choose and isfile(fname) and not force:
        text = f'suggested file name: {fname}\n       want to load? '
    if text: # use text itself as a flag
        choice = input(text).lower()
        if choice == 'yes' or choice == 'y' or choice == '':
            return fname
    if (not isfile(fname) or choose) and not force:
        text = (not isfile(fname))*f'{fname} does not exists!\n'+\
                                    'type right filename: '
        return check_load(input(text),choose=False)
    if not isfile(fname):
        print(f'warning: {fname} does not exists')
        return None
    return fname
    
def colors(N,cmap=DEF_CMAP):
    """
    Proper color cycle.

    Parameters
    ----------
    N : int
        Number of distinct colors to be produced.
    cmap : str
        Matplotlib colormap name.
    """
    if N>1:
        intervals = arange(0,1+1/(N-1),1/(N-1))
    else:
        intervals = [0.5]
    return [plt.get_cmap(cmap)(i) for i in intervals]

def short(selection_name):
    """
    Provide underscore-tied short name for a selection.
    """
    return '_'.join([word for word in selection_name.split(' ')\
                     if word not in MUTE_WORDS])[0:DEF_N]\
                     *(selection_name != DEF_SEL)

def real_selection_name(selection_name,delimiter=' ()'):
    """
    Interpret "selection_name" in a way which is understandable by the program.
    """
    just_words = replace_chars(selection_name,delimiter)
    just_punctuation = keep_chars(selection_name,delimiter)
        
    just_words = [set_optional(ALIAS,word,word) for word in just_words]
    just_words = [f'({text})' if len(text.split())>1 else text\
                                                for text in just_words]
    return ''.join([just_punctuation[i]+just_words[i]\
                    for i in range(len(just_words))]+\
                    just_punctuation[len(just_words):])
    
def replace_chars(text,chars='',replacement=' ',make_list=True):
    if len(chars):
        return replace_chars(text.replace(chars[0],replacement),
                             chars[1:],replacement)
    return text.split() if make_list else text
    
def keep_chars(text,chars='',make_list=True):
    result = ['']
    for x in text:
        if x in chars:
            result[-1] += x
        else:
            result.append('')
    return [result[0]]+[x for x in result[1:] if len(x)]\
           if make_list else ''.join(result)

def sigmoid(x, x0, k, m, n): 
    y = m / (1 + exp(k*(x-x0))) + n
    return y
    
# pyfferaph

def _sbmatrix(universe,selection,cutoff=4.5,verbose=True):
    """
    Builds salt bridges matrix for pyfferaph.
    """
    indices = ' '.join([str(x) for x in selection.indices])
    selection_acidic =\
    real_selection_name(f'{ALIAS["acidic"]} and index {indices}')
    selection_basic  =\
    real_selection_name(f'{ALIAS["basic"]} and index {indices}')
    acidic = universe.select_atoms(selection_acidic)
    basic  = universe.select_atoms(selection_basic)
    n = len(universe.residues)
    matrix = zeros((n,n),int)
    print(verbose*'building salt bridges matrix\n',end='')
    table = []
    for frame in tqdm(universe.trajectory,disable=not verbose):
        d = capped_distance(acidic.positions,basic.positions,
        max_cutoff=cutoff,return_distances=False,box=acidic.dimensions)
        t = list(set(\
        [(acidic[i[0]].resindex,basic[i[1]].resindex)\
        if acidic[i[0]].resindex<basic[i[1]].resindex else\
        (basic[i[1]].resindex,acidic[i[0]].resindex) for i in d]))
        # now if 1->2, 2->3 and 3->2 we'll have (1,2),(2,3)
        table += t
    for r1,r2 in table:
        matrix[r1,r2] += 1 if r2!=r1 else 0
    # now the matrix is upper diagonal: make it symm
    matrix = (matrix+matrix.T)/len(universe.trajectory)
    matrix = matrix[sorted(list(set(selection.resindices))),:]\
                   [:,sorted(list(set(selection.resindices)))]
    return matrix
    
def _hcmatrix(universe,selection,cutoff=5.51,verbose=True):
    indices = ' '.join([str(x) for x in selection.indices])
    selection_name = '(hydrophobic and (not backbone) or '+\
                    f'(resname DGP* and (chain1 or chain2))) and index {indices}'
    sel = universe.select_atoms(real_selection_name(selection_name))
    n = len(universe.residues)
    matrix = zeros((n,n),int)
    print(verbose*'building hydrophobic contacts matrix\n',end='')
    table = []
    for frame in tqdm(universe.trajectory,disable=not verbose):
        d, _ = self_capped_distance(sel.center_of_mass\
                                   (compound='residues',pbc=True),
                                    max_cutoff=cutoff,box=selection.dimensions)
        t = list(set(\
        [(sel.residues[i[0]].resindex,sel.residues[i[1]].resindex)\
        if sel.residues[i[0]].resindex<sel.residues[i[1]].resindex else\
        (sel.residues[i[1]].resindex,sel.residues[i[0]].resindex) for i in d]))
        table += t
    for r1,r2 in table:
        matrix[r1,r2] += 1 if r2!=r1 else 0
    # now the matrix is upper diagonal: make it symm
    matrix = (matrix+matrix.T)/len(universe.trajectory)
    matrix = matrix[sorted(list(set(selection.resindices))),:]\
                   [:,sorted(list(set(selection.resindices)))]
    return matrix
    
def _hbmatrix(universe,selection,dist=3.5,angle=120.0,verbose=True):
    indices = ' '.join([str(x) for x in selection.indices])
    sel1_name = f'protein and index {indices}'
    sel2_name = f'(protein or membrane or calcium) and index {indices}'
    lipidic_acceptors = ['O12','O13','O14','O11','O21',
                         'O22','O31','O32','O13A','O13B']
    lipidic_donors = []
    n = len(universe.residues)
    matrix = zeros((n,n),int)
    print(verbose*'building hydrogen bonds contacs matrix\n',end='')
    hb_analysis = hbonds.HydrogenBondAnalysis(universe,
                  real_selection_name(sel1_name),real_selection_name(sel2_name),
                  update_selection1=False,update_selection2=False,distance=dist,
         angle=angle,acceptors=lipidic_acceptors,donors=lipidic_donors,pbc=True)
    hb_analysis.run()
    hb_analysis.generate_table()
    t = hb_analysis.table
    table =[(t['time'][i],universe.atoms[t['donor_index'][i]].resindex,
                          universe.atoms[t['acceptor_index'][i]].resindex)\
                       if universe.atoms[t['donor_index'][i]].resindex <\
                          universe.atoms[t['acceptor_index'][i]].resindex else\
            (t['time'][i],universe.atoms[t['acceptor_index'][i]].resindex,
                          universe.atoms[t['donor_index'][i]].resindex)\
                           for i in tqdm(range(len(t)),disable=not verbose)]
    table = list(set(table))
    table = [(t[1],t[2]) for t in table]
    for r1,r2 in table:
        matrix[r2,r1] += 1 if r2!=r1 else 0
    matrix = (matrix+matrix.T)/len(universe.trajectory)
    matrix[matrix>1]=1 # double donor-acceptor
    matrix = matrix[sorted(list(set(selection.resindices))),:]\
                   [:,sorted(list(set(selection.resindices)))]
    return matrix
    
def matrix_difference(matrix1,matrix2):
    if len(matrix1) != len(matrix2):
        return
    if len(matrix1) != len(matrix1[0]):
        return
    if len(matrix2) != len(matrix2[0]):
        return
    bigger1 = []
    bigger2 = []
    for i in range(len(matrix1)):
        for j in range(i+1,len(matrix2)):
            if   matrix1[i,j]>matrix2[i,j]:
                bigger1.append((i,j))
            elif matrix2[i,j]<matrix2[i,j]:
                bigger2.append((i,j))
    return bigger1,bigger2
  
     
class system:
    """
    Main class.
    """
    def __init__(self,input_name,**args):
        """
        Parameters
        ----------
        input_name : str
            .pdb or .gro file. Extension is not necessary if the file is .pdb.
            Anyway, mistaken or not, it can always be corrected.
        input_traj : str, optional
            Either a .dcd file or a list of .dcd file. In the first case,
            mistaken or not, it can always be corrected.
        input_folder : str, optional
            A shortcut to avoid lengthy "input_name", "input_traj" and
            "output_name" definitions.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself shall be taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            It is worth only when "reference_name" is not specified, so that the
            "input_name" universe itself shall be taken as reference.
        start : int, optional
            The frame to start with if loading the "input_name" universe.
        step : int, optional
            The frame interval if loading the "input_name" universe.
        stop : int, optional
            The frame to end at if loading the "input_name" universe.
        name : str, optional
            Fancy name which defines the system. If not specified, "input_name"
            shall be chosen instead.
        """
        # parameters
        self.input_name      = str(input_name)
        self.name            = set_optional(args,'name','')
        self.input_traj      = set_optional(args,'input_traj','')
        self.input_folder    = set_optional(args,'input_folder','')
        self.output_name     = set_optional(args,'output_name',input_name)
        self.selection_name  = set_optional(args,'selection_name','')
        self.reference_name  = set_optional(args,'reference_name','')
        self.reference_frame = set_optional(args,'reference_frame',DEF_REF)
        self.start           = set_optional(args,'start',0)
        self.step            = set_optional(args,'step',+1)
        self.stop            = set_optional(args,'stop',-1)
        # universe
        self.__backup  = None # backup copy (useful when changing frames etc.)
        self.universe  = None
        self.reference = None
        self.universe_selection  = None
        self.reference_selection = None  
        self.dt = DEF_DT # default timesteps between frames (ps)
        self.__aligned = False
        # analysis results
        self.__pyff = None
        self.__gyr  = None
        self.__pca  = None
        self.__rmsd = None
        self.__rmsf = None # output of rmsf
        self.__dist = None
        
    def __str__(self):
        if self.universe is None:
            return 'empty universe'
        return   'universe with: '+\
              f'{self.universe.trajectory.n_frames} frames'+\
              f'\n        atoms: '+\
              f'{self.universe.select_atoms("all").n_atoms}'+\
              f'\n      protein: '+\
              f'{self.universe.select_atoms(ALIAS["protein"]).n_atoms}'+\
              f'\n     membrane: '+\
              f'{self.universe.select_atoms(ALIAS["membrane"]).n_atoms}'+\
              f'\n      calcium: '+\
              f'{self.universe.select_atoms(ALIAS["calcium"]).n_atoms}'+\
              f'\n        water: '+\
              f'{self.universe.select_atoms(ALIAS["water"]).n_atoms}'
              
    def pyff_compare(self,matrix,verbose=True):
        """
        If pyfferaph matrix is computed: see the difference with an other
        matrix.
        """
        if self.__pyff is None:
            return
        bigger1,bigger2=matrix_difference(self.__pyff,matrix)
        for i,j in bigger1:
            print(verbose*f'interaction '+\
                  f'{self.universe_selection.residues[i]}'+\
                  f' - {self.universe_selection.residues[j]} found in '+\
                  f'system matrix and not in other\n',end='')
        for i,j in bigger2:
            print(verbose*f'interaction '+\
                  f'{self.universe_selection.residues[i]}'+\
                  f' - {self.universe_selection.residues[j]} found in '+\
                  f'other matrix and not in system\n',end='')
        return len(bigger1)+len(bigger2)
        
    def pca_project(self,selection,n_pca=DEF_PCA):
        """
        Returns projections according to stored pca.
        """
        if self.__pca is None:
            return
        n_frames = selection.universe.trajectory.n_frames
        projection=zeros((n_frames,n_pca))
        mean_pos = zeros(len(self.__pca)) # mean position
        for frame in selection.universe.trajectory:
            mean_pos += selection.positions.ravel()
        mean_pos *= 1/n_frames
        for i,frame in enumerate(selection.universe.trajectory):
            pos = selection.positions.ravel()-mean_pos
            projection[i,:] = dot(pos,self.__pca[:,:n_pca])
        T = array([[self.dt*i] for i in range(len(projections))])
        results = concatenate((T,projections),axis=1)
        return results
    def fname(self,name=None,selection_name=None):
        """
        Provide default file address for loading/saving, with neither suffix
        nor extension.

        Parameters
        ----------
        name : str, default None
            Prefix of the file name. If None: takes "self.output_name".
        selection_name : str, default None
            Builds selection suffix upon "selection_name". If None: takes
            "self.selection_name".
            
        Returns
        -------
        fname : str
            
        See Also
        --------
        system.short : related method
        """
        name  = name if name else self.output_name
        sel   = selection_name if type(selection_name) is str\
                             else self.selection_name
        fname = (self.input_folder!='')*f'{self.input_folder}/'+name
        fname = fname[:-4] if fname[-4:] == '.pdb' or fname[-4:] == '.gro'\
                or fname[:-4] == '.dcd' or fname[:-4] =='.csv' else fname        
        return fname+'_'+short(sel) if short(sel) else fname
    def load(self,save=False,**args):
        """
        Load the "input_name" universe, the "reference_name" universe and the
        corresponding selections. Do nothing if it would change nothing.
        
        Parameters
        ----------
        save : bool, default is False
            If True: save universe selection .pdb and .dcd to file.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start : int, optional
            The frame to start with when loading the "input_name" universe.
        step : int, optional
            The frame interval when loading the "input_name" universe.
        stop : int, optional
            The frame to end at when loading the "input_name" universe.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is False, False.
        verbose : bool, optional
            Default is False.
        """
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        # create a backup universe if it doesn't already exist
        if self.__backup is None:
            print(verbose*'loading main universe data\n',end='')
            pdb = self.fname(input_name,'')+'.pdb'
            pdb = check_load(pdb,choose=choose,force=force)
            if not self.input_traj:
                dcd = '.'.join(pdb.split('.')[:-1]) + '.dcd'
                dcd = check_load(dcd,choose=choose,force=force)
            elif type(self.input_traj) is str:
                dcd = self.fname(input_traj,'')+'.dcd'
                dcd = check_load(dcd,choose=choose,force=force)
            else:
                dcd = [self.fname(dcd,'')+'.dcd' for dcd in self.input_traj]
            print(verbose*f'>>> {pdb}\n>>> {dcd}\n',end='')
            self.__backup = Universe(pdb,dcd) # backup universe
            self.input_name   = pdb.split('/')[-1] # update infos
            self.input_folder =  '/'.join(pdb.split('/')[0:-1])
            self.input_traj   = ['/'.join(dcd.split('/')[0:-1])\
                                 for dcd in dcd] if type(dcd) is list else\
                                 '/'.join(dcd.split('/')[0:-1])
        # load universe if needed
        if not (self.start==start and self.step==step and self.stop==stop)\
        or not self.universe:
            self.universe  = self.__backup.copy()
            self.start = start # update parameters
            self.stop = stop
            self.step = step
            stop = stop  if stop > 0 else self.__backup.trajectory.n_frames
            print(verbose*f'loading {int((stop-start)/step)} frames\n',end='')
            self.universe.transfer_to_memory(start=start,step=step,stop=stop,\
                                             verbose=verbose)
            self.dt = self.universe.trajectory.dt/1e2
        # load reference if needed
        if not self.reference_name==reference_name or not self.reference:
            print(verbose*'loading reference data\n',end='')
            if reference_name:
                pdb = self.fname(reference_name,'')+'.pdb'
                pdb = pdb if pdb[-4:]=='.pdb'or pdb[-4:]=='.gro' else pdb+'.pdb'
                pdb = check_load(pdb,choose=choose,force=force)
                print(verbose*f'>>> {pdb}\n',end='')
                self.reference = Universe(pdb)
                self.__aligned = False # need to align if requested
            self.reference_name = reference_name # update parameter
        # load ref frame only when the main universe itself is taken as ref 
        if (not self.reference_frame==reference_frame and not reference_name)\
         or not self.reference:
            self.__aligned = False # need to align if requested
            self.reference = self.__backup.copy()
            self.reference_frame = reference_frame # update parameter
            reference_frame = reference_frame if reference_frame >= 0 else\
                              self.reference.trajectory.n_frames-1
            print(verbose*'extracting reference frame ',end='')
            print(verbose*f'{reference_frame}\n',end='')
            self.reference.transfer_to_memory(start=reference_frame,
                                              stop =reference_frame+1)
            if reference_frame == -2:
                # allinea a zero
                pass
        # always load selections
        if not self.selection_name == selection_name:
            print(verbose*f'loading selection "{selection_name}"\n',end='')
        sel = real_selection_name(selection_name)
        self.universe_selection  = self.universe.select_atoms(sel)
        self.reference_selection = self.reference.select_atoms(sel)
        self.selection_name = selection_name # update parameter
        if align and not self.__aligned:
            print(verbose*f'aligning system to reference\n',end='')
            self.__aligned = True
            AlignTraj(self.universe,self.reference,select=sel,
                      weights='mass',in_memory=True,verbose=verbose).run()
        # save pdb/dcd
        if save:
            print(verbose*'extracting .pdb of selection ',end='')
            print(verbose*f'"{selection_name}"\n',end='')
            fname = check_save(f'{self.fname(output_name)}.pdb',
                               choose=choose,force=force)
            self.universe_selection.write(fname)
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*f'extracting {self.universe.trajectory.n_frames} '+\
                  verbose*f'frames of selection "{selection_name}"\n',end='')
            fname = '.'.join(fname.split('.')[:-1])
            fname = check_save(f'{fname}.dcd',choose=choose,force=force)
            with Writer(fname,self.universe_selection.n_atoms) as W:    
                for frame in self.universe.trajectory:
                    W.write(self.universe_selection)
            print(verbose*f'>>> {fname}\n',end='')
    def gyr(self,compute=True,save=True,plot=True,**args):
        """
        Return the gyration radius of "self.universe_selection".

        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute gyration radius.
            If True and system data are not loaded: ask wheter to load a .csv.
            If False: load a .csv.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        verbose : bool, optional
            Default is False.
        output : bool, optional
            If False: don't return anything. Default is True.

        Returns
        -------
        T,results : tuple
            "T" is the time vector, "results" is the gyration radius vector.
        """
        # parameters
        fname  = ''
        suffix = '_gyr'
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'computing gyration radius for ',end='')
                print(verbose*f'selection "{selection_name}"\n',end='')
                results = [self.universe_selection.radius_of_gyration()\
                for frame in tqdm(self.universe.trajectory,disable=not verbose)]
                T  = [x*self.dt for x in range(len(results))]
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}.csv',choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            self.dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= ', radius of gyration'
            title = set_optional(args,'title',default_title)
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted gyration radius; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__gyr = results
        if output:
            return T,results
    def rmsd(self,compute=True,save=True,plot=True,**args):
        """
        Return the rmsd of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute rmsd.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,results,results_residues : tuple
            "T" is the time vector, "results" is the rmsd vector,
            "results_residues" is the "(residue_name,rmsd2 vector)"
            list of tuples.
        """
        # parameters
        fname  = ''
        suffix = '_rmsd'
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'aligning system traj to reference\n',end='')
                sel = real_selection_name(selection_name)
                print(verbose*'computing rmsd for selection ',end='')
                print(verbose*f'"{selection_name}"\n',end='')
                results = [rmsd(self.universe_selection.positions,\
                                self.reference_selection.positions)\
                for frame in tqdm(self.universe.trajectory,disable=not verbose)]
                T  = array([x*self.dt for x in range(len(results))])
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
                else:
                    return
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}.csv',choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            self.dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}'\
                            if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= ', RMSD with respect to '
            default_title+= reference_name if reference_name else\
                            f'frame {reference_frame//self.step}' if\
                            reference_frame>=0 else f'last frame'
            title = set_optional(args,'title',default_title)
            figure()
            T=T[results<cut]
            results=results[results<cut]
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('RMSD [A]')
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted rmsd; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__rmsd = results
        if output: 
            return T,results
    def rmsf(self,compute=True,save=True,plot=True,**args):
        """
        Return the rmsf of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute rmsd.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,results,results_residues : tuple
            "T" is the time vector, "results" is the rmsd vector,
            "results_residues" is the "(residue_name,rmsd2 vector)"
            list of tuples.
        """
        # parameters
        fname  = ''
        suffix = '_rmsf'
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'computing rmsf for selection ',end='')
                print(verbose*f'"{selection_name}"\n',end='')
                results=RMSF(self.universe_selection,verbose=verbose).run().rmsf
                results_residues = []
                residues = []
                print(verbose*'computing rmsf by residue\n',end='')
                for residue in tqdm(self.universe_selection.residues,
                                disable=not verbose):
                    residues.append(residue.resname)
                    results_residues.append(mean(RMSF(residue.atoms).run().rmsf))
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,results,delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving residues dictionary to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}'+\
                                      '_residues.dic',choose=choose,force=force)
                    savetxt(fname,residues,fmt='%s')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving rmsf by residue data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                      '_residues.csv',choose=choose,force=force)
                    savetxt(fname,results_residues)
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
                else:
                    return
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}.csv',choose=choose,force=force)
            results = loadtxt(fname)
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading residues dictionary\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                              f'{suffix}_residues.dic',choose=choose,force=force)
            with open(fname) as f:
                residues = [res.replace('\n','') for res in f.readlines()]
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading rmsf2 by residue data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                              f'_residues.csv',choose=choose,force=force)
            results_residues = loadtxt(fname)
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= ', RMSF by residue'
            title = set_optional(args,'title',default_title)
            figure(figsize=(8.533,4.8))
            plt.grid(zorder=0)
            plt.xlim(0,len(residues))
            d = max(results_residues)-min(results_residues)
            plt.ylim(max(0,min(results_residues)-d*.1),
                     max(results_residues)+d*.3)
            residues_dictionary={}
            for i,residue in enumerate(residues):
                if residue in residues_dictionary:
                    residues_dictionary[residue].append(i)
                else:
                    residues_dictionary[residue] = [i]
            col = colors(len(residues_dictionary))
            for i,residue in enumerate(residues_dictionary):
                plt.bar([j+1/2 for j in residues_dictionary[residue]],\
          [results_residues[j] for j in residues_dictionary[residue]],\
                        width=1,color=col[i],zorder=4,label=residue)
            plt.legend(loc='upper center',\
                       ncol=int(len(residues_dictionary)/3),fontsize=9)
            plt.xlabel('residues')
            plt.ylabel('RMSF [A]')      
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted rmsf by residue; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__rmsf = results_residues
        if output: 
            return results,\
            [(residues[i],results_residues[i]) for i in range(len(residues))]
    def pca(self,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
        # parameters
        fname  = ''
        suffix = '_pca'
        # auxiliary function
        def projection_plot(h,k,z=None,fname='',title=''): # plot proj. h vs k
            title+=f', components {h+1} vs {k+1}'
            gs=GridSpec(1,2)
            figure(figsize=(8.533,4.8))
            col=colors(len(T),DEF_CMAP2)
            if not z:
                a1=plt.subplot(gs[0])
                a2=plt.subplot(gs[1]) # barplot
                [a1.plot(projections[i,h],projections[i,k],'.',color=col[i])\
                for i in tqdm(range(len(T)),disable=not verbose)]
            else:
                title += f' vs {z+1}'
                a1=plt.subplot(gs[0],projection='3d')
                a2=plt.subplot(gs[1]) # barplot
                [a1.plot([projections[i,h]],[projections[i,k]],\
                         [projections[i,z]],'.',color=col[i])\
                for i in tqdm(range(len(T)),disable=not verbose)]  
                a1.set_zlabel(f'component {z+1}')
            a1.grid()
            a1.set_xlabel(f'component {h+1}')
            a1.set_ylabel(f'component {k+1}')
            a1.set_title(title)
            img = a2.imshow([[0,0]],cmap=DEF_CMAP2,
                            vmin=T.min(),vmax=round(T.max()+self.dt))
            plt.gca().set_visible(False)
            cb=plt.colorbar(img,ax=a2,ticks=[i for i in range(0,400,100)])
            cb.ax.set_yticklabels([f'{i} ns' for i in range(0,400,100)])
            a1.set_position([0.11,0.12,0.72,0.75])
            a2.set_position([0.72,0.16,0.90,0.75])
            plt.grid()
            plt.title(title)
            draw()
            pause()
            print(verbose*f'plotted PCA component 1 vs 2; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = '_'.join(fname.split('_')[:-1])+f'_{h+1}{k+1}'
                fname+= f'{z+1}.pdf' if z else '.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            return fname

        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'aligning system traj to selection\n',end='')
                print(verbose*'computing principal components for ',end='')
                print(verbose*f'selection "{selection_name}"\n',end='')
                sel = real_selection_name(selection_name)
                pca = PCA(self.universe,select=sel,
                          verbose=verbose).run()
                eigenvalues = abs(pca.variance)
                components  = pca.p_components
                self.__pca  = components
                print(verbose*f'computing {n_pca} projections\n',end='')
                projections=self.pca_project(self.universe_selection,
                                             n_pca=n_pca)
                T = array([[self.dt*i] for i in range(len(projections))])
                results = concatenate((T,projections),axis=1)
                if save: # saving
                    print(verbose*'saving projections to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}'+\
                            f'{suffix}.csv',choose=choose,force=force)
                    savetxt(fname,results,delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving components to file\n',end='')
                    fname = check_save(fname[:-4]+\
                            '_components.csv',choose=choose,force=force)
                    savetxt(fname,transpose(components),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving eigenvalues to file\n',end='')
                    fname = check_save('_'.join(fname.split('_')[:-1])+\
                            '_eigenvalues.csv',choose=choose,force=force)
                    savetxt(fname,eigenvalues)
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[],[]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading projections from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                           f'{suffix}.csv',choose=choose,force=force)
            results = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading eigenvalues from database\n',end='')
            fname = check_load(fname[:-4]+'_eigenvalues.csv',
                               choose=choose,force=force)
            eigenvalues = loadtxt(fname)
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading components from database\n',end='')
            fname = check_load('_'.join(fname.split('_')[:-1])+\
                               '_components.csv',choose=choose,force=force)
            components = transpose(loadtxt(fname,delimiter=','))
            self.__pca = components
            print(verbose*f'>>> {fname}\n',end='')
            T = results[:,0] # retrieve T and projections
            projections = results[:,1:]
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)} PCA'\
                            if short(selection_name) else ' PCA'
            title = set_optional(args,'title',default_title)
            title+= f', first {DEF_EIG} eigenvalues'
            figure()
            plt.grid(zorder=0)
            plt.xlim(0,DEF_EIG)
            plt.bar(arange(DEF_EIG)+0.5,eigenvalues[0:DEF_EIG],width=1,zorder=3)
            plt.xticks(arange(DEF_EIG)+0.5,[f'{i+1}' for i in range(DEF_EIG)])
            plt.ylabel('eigenvalue')
            plt.title(title)
            draw()
            pause()
            print(verbose*f'plotted first {DEF_EIG} eigenvalues; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname.split('_components')[0].split('.pdf')[0] if fname\
                        else f'{self.fname(output_name)}{suffix}'
                fname = check_save(fname+\
                                   '_eigenvalues.pdf',choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            default_title = f'{self.name}' if self.name else \
                    f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)} PCA'\
                            if short(selection_name) else ' PCA'
            title = set_optional(args,'title',default_title)
            for h in range(0,n_pca-1):
                for k in range(h+1,n_pca):
                    fname = projection_plot(h,k,fname=fname,title=title)
            if n_pca>=3:
                projection_plot(0,1,2,fname,title)
        if output:
            return results,eigenvalues,components
            


    def pyff(self,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
        # parameters
        fname  = ''
        suffix = '_pyff'
        # auxiliary functions
        def matplot(matrix,fname='',title='',cbar=True):
            figure()
            ax = plt.axes()
            for i,segment in enumerate(set(segments)):
                col = plt.get_cmap('Set1')(i)
                for j in range(len(segments)):
                    if segments[j]==segment:
                        ax.add_patch(patches.Rectangle((j,0),1,len(matrix),lw=0,
                        color=col,alpha=0.2))
                        ax.add_patch(patches.Rectangle((0,j),len(matrix),1,lw=0,
                        color=col,alpha=0.2))
                ax.add_patch(patches.Rectangle((-1,-1),1,1,lw=0,
                        color=col,alpha=0.2,label=segment))
            heatmap(matrix,square=True,cmap='binary',cbar=cbar)
            plt.xlabel('residues')
            plt.ylabel('residues')
            plt.xticks([])
            plt.yticks([])
            plt.subplots_adjust(top=0.92)
            plt.suptitle(title)
            if i>0:
                pos = ax.get_position()
                pos.x0 += 0.1
                ax.set_position(pos)
                plt.legend(bbox_to_anchor=(-0.067,1))
            draw()
            pause()
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            return fname
        def filter_threshold(matrix,fname,title):
            print(verbose*'filtering interactions\n',end='')
            edges_size = []
            maxclustsize = [] # size of biggest cluster according to thresh
            for val in DEF_VALS:       
                boolmat = matrix>val
                G=Graph(boolmat)
                maxclustsize.append(len(max(connected_components(G),key=len)))
                edges_size.append(len(G.edges))
            fit = curve_fit(sigmoid,DEF_VALS,maxclustsize,p0=(0.5,50,50,1),
                            maxfev=1000000)
            flex = fit[0][0]
            if flex > min(DEF_VALS) and flex < max(DEF_VALS):
                print(f'> found flex at: {flex:.3f}')
            else:
                print('found negative flex: '+\
                'looking for max gap in maxclustsize instead')
                flex = DEF_VALS[argmax(-diff(maxclustsize))+1]
                print(f'> found critical value at: {flex:.3f}')
            figure()
            plt.grid()
            plt.plot(DEF_VALS,maxclustsize,'.',label='max clust size')
            plt.plot(DEF_VALS,edges_size,dashes=[2,2],label='n of edges')
            d = max(edges_size)-min(maxclustsize)
            plt.xlim(min(DEF_VALS),max(DEF_VALS))
            plt.ylim(min(maxclustsize)-d*.1,max(edges_size)+d*.3)
            plt.xlabel("$p_{min}$")
            plt.ylabel("size")
            x = linspace(min(DEF_VALS),max(DEF_VALS))
            plt.plot(x,sigmoid(x,*fit[0]),label='fit')
            plt.title(title)
            draw()
            pause()
            while True:
                try:
                    choice = input('> choose your threshold'+\
                                   '(enter to confirm): ')
                    flex = float(choice) if len(choice) else flex
                    i = len([1 for val in DEF_VALS if val<flex])
                    break
                except:
                    pass
            plt.plot(flex,sigmoid(flex,*fit[0]),'o',\
                     label=f'threshold ({flex:.2f},{sigmoid(flex,*fit[0]):.2f})', 
                     color='red')
            plt.title(title+f': filtered {edges_size[i]} contacts')
            plt.legend()
            print(verbose*'plotted fit of persistence threshold\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            boolmat = matrix>flex
            return boolmat
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'pyfferaph for selection ',end='')
                print(verbose*f'"{selection_name}"\n',end='')
                sbmatrix = _sbmatrix(self.universe,
                                     self.universe_selection,verbose=verbose)
                hcmatrix = _hcmatrix(self.universe,
                                     self.universe_selection,verbose=verbose)
                hbmatrix = _hbmatrix(self.universe,
                                     self.universe_selection,verbose=verbose)
                print(verbose*'retrieving segments list\n',end='')
                segments =\
                [residue.segid for residue in self.universe_selection.residues]
                if save: # saving
                    print(verbose*'saving salt bridges matrix to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                       '_sbmatrix.csv',choose=choose,force=force)
                    savetxt(fname,transpose(sbmatrix),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving hydrophobic contacts to file\n',end='')
                    fname = check_save(fname.split('_sbmatrix.csv')[0]+\
                                       '_hcmatrix.csv',choose=choose,force=force)
                    savetxt(fname,transpose(hcmatrix),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving hydrogen bonds matrix to file\n',end='')
                    fname = check_save(fname.split('_hcmatrix.csv')[0]+\
                                       '_hbmatrix.csv',choose=choose,force=force)
                    savetxt(fname,transpose(hbmatrix),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving segments dictionary to file\n',end='')
                    fname = check_save(fname.split('_hbmatrix.csv')[0]+\
                                       '_segments.dic',choose=choose,force=force)
                    savetxt(fname,segments,fmt='%s')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [[]]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [[]]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading salt bridges from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}_sbmatrix.csv',choose=choose,force=force)
            sbmatrix = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading hydrophobic contacs from database\n',end='')
            fname = check_load(fname.split('_sbmatrix.csv')[0]+\
                               f'_hcmatrix.csv',choose=choose,force=force)
            hcmatrix = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading hydrogen bonds from database\n',end='')
            fname = check_load(fname.split('_hcmatrix.csv')[0]+\
                               f'_hbmatrix.csv',choose=choose,force=force)
            hbmatrix = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading segments dictionary\n',end='')
            fname = check_load(fname.split('_hbmatrix.csv')[0]+\
                               f'_segments.dic',choose=choose,force=force)
            with open(fname) as f:
                segments = [x.replace('\n','') for x in f.readlines()]
            print(verbose*f'>>> {fname}\n',end='')
        default_title = f'{self.name}' if self.name else \
                        f'{self.input_name.split("/")[-1]}'
        default_title+= f' {short(selection_name)}'\
                        if short(selection_name) else ''
        title = set_optional(args,'title',default_title)
        if plot: # plotting
            fname = '_'.join(fname.split('_')[:-1])+f'_sbmatrix.pdf'
            fname = matplot(sbmatrix,fname,
                            title+', salt bridges interaction matrix')
            fname = '_'.join(fname.split('_')[:-1])+f'_hcmatrix.pdf'
            fname = matplot(hcmatrix,fname,
                            title+', hydrophobic contacts interaction matrix')
            fname = '_'.join(fname.split('_')[:-1])+f'_hbmatrix.pdf'
            fname = matplot(hbmatrix,fname,
                            title+', hydrogen bonds interaction matrix')
        fname='_'.join(fname.split('_')[:-1])
        matrix = filter_threshold(sbmatrix,fname+'_sbfilter.pdf',
                                           title+', salt bridges')
        matrix+= filter_threshold(hcmatrix,fname+'_hcfilter.pdf',
                                           title+', hydrophobic contacts')
        matrix+= filter_threshold(hbmatrix,fname+'_hbfilter.pdf',
                                           title+', hydrogen bonds')
        matrix = matrix>0
        if plot:
            fname = matplot(matrix,fname+'.pdf',
                            title+', full interaction matrix',cbar=False)
        self.__pyff = matrix
        if output:
            return matrix
    def dist(self,sel,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        sel : str
            Selection name (to be aligned with self.universe_selection).
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
        # parameters
        fname  = ''
        suffix = '_dist'
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        remove = []
        if 'x' not in axis:
            remove.append(0)
        if 'y' not in axis:
            remove.append(1)
        if 'z' not in axis:
            remove.append(2)
        sel_name  = real_selection_name(sel)
        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                if len(remove) == 3:
                    print(verbose*'warning: wrong "axis" argument\n',end='')
                    return [],[]
                print(verbose*'computing distance for ',end='')
                print(verbose*f'selection "{selection_name} ',end='')
                print(verbose*f'vs selection "{sel}"',end='')
                print(verbose*(len(remove)<3 and len(remove)>0)\
                             *f', projection on the {axis} ',end='')
                print(verbose*(len(remove)==1)*'plane\n',end='')
                print(verbose*(len(remove)==2)*'axis\n',end='')
                selection = self.universe.select_atoms(sel_name)
                results = []
                for frame in tqdm(self.universe.trajectory,disable=not verbose):
                    x = self.universe_selection.center_of_mass()
                    y = selection.center_of_mass()
                    x[remove] *= 0
                    y[remove] *= 0
                    results.append(module(x-y))
                T  = [x*self.dt for x in range(len(results))]
                if save: # saving
                    print(verbose*'saving data to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                   f'_{short(sel)}'+\
                                   f'{"_"+axis if len(remove) else ""}.csv',\
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading data from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}_{short(sel)}'+\
                            f'{"_"+axis if len(remove) else ""}.csv',
                                                  choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            self.dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            print(verbose*f'>>> {fname}\n',end='')
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            default_title+= f' vs {short(sel)} distance'
            title = set_optional(args,'title',default_title)
            title += f', proj. on the {axis} plane'\
                     if len(remove)==1 else\
                     f', proj. on the {axis} axis'
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            draw()
            pause()
            print(verbose*'plotted distance; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}_{short(sel)}'+\
                            f'{"_"+axis if len(remove) else ""}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
        self.__dist = results
        if output:
            return T,results
    def log(self,log='log.log',k=[],save=True,**args):
        """
        Get info from vmd log, of keys in "k".
        """
        fname = ''
        suffix = '_log'
        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
        k = [k.lower() for k in k]
        dcdfreq = None
        keys    = None
        data    = None
        ts      = None
        steps   = 0
        J = [] # keys to be kept
        with open((len(input_folder)>0)*f'{input_folder}/'+log) as f:
            for line in f:
                if   not ts      and 'Info: TIMESTEP' in line:
                    ts      = float(line.split()[-1])/1e6 # step in ns
                elif not dcdfreq and 'Info: DCD FREQUENCY' in line:
                    dcdfreq = int(line.split()[-1])
                elif not keys    and line[0:7] == 'ETITLE:':
                    keys    = line.split()[2:]
                    data    = {'T':[]}
                    for j in range(len(keys)):
                        if keys[j].lower() in k or len(k)==0:
                            J.append(j)
                            data[keys[j]] = []
                if keys and dcdfreq and ts:
                    break
            self.dt = dcdfreq*ts*step
            T=[]
            i = 1
            for line in f:
                # fill keys
                if line[0:7] == 'ENERGY:':
                    fields = line.split()
                    if int(fields[1]) != i*dcdfreq*step:
                        continue
                    fields = fields[2:]
                    T.append(i*self.dt)
                    data['T'].append(T[-1])
                    for j in J:
                        data[keys[j]].append(float(fields[j]))
                    i += 1
        fname = fname.split('.pdb')[0] if fname\
                else f'{self.fname(output_name)}{suffix}'
        for key in data:
            if key == 'T':
                continue
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)}'\
                            if short(selection_name) else ''
            title = set_optional(args,'title',default_title)
            title += f' {key.lower()}'
            figure()
            results = data[key]
            plt.plot(T,results,'.')
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{2*n*self.dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel(f'{key.lower()}')
            plt.title(title)
            draw()
            pause()
            print(verbose*f'plotted {key.lower()}; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = check_save(fname+f'_{key}.pdf',choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
                fname=fname.split(f'_{key}.pdf')[0].split(f'{key}.pdf')[0].\
                            split('.pdf')[0]
        if output:
            return data
