        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'aligning system traj to selection\n',end='')
                print(verbose*'computing principal components for ',end='')
                print(verbose*f'selection "{selection_name}"\n',end='')
                sel = real_selection_name(selection_name)
                pca = PCA(self.universe,select=sel,
                          verbose=verbose).run()
                eigenvalues = abs(pca.variance)
                components  = pca.p_components
                self.__pca  = components
                print(verbose*f'computing {n_pca} projections\n',end='')
                projections=self.pca_project(self.universe_selection,
                                             n_pca=n_pca)
                T = array([[self.dt*i] for i in range(len(projections))])
                results = concatenate((T,projections),axis=1)
                if save: # saving
                    print(verbose*'saving projections to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}'+\
                            f'{suffix}.csv',choose=choose,force=force)
                    savetxt(fname,results,delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving components to file\n',end='')
                    fname = check_save(fname[:-4]+\
                            '_components.csv',choose=choose,force=force)
                    savetxt(fname,transpose(components),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving eigenvalues to file\n',end='')
                    fname = check_save('_'.join(fname.split('_')[:-1])+\
                            '_eigenvalues.csv',choose=choose,force=force)
                    savetxt(fname,eigenvalues)
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [],[],[]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [],[],[]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading projections from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                           f'{suffix}.csv',choose=choose,force=force)
            results = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading eigenvalues from database\n',end='')
            fname = check_load(fname[:-4]+'_eigenvalues.csv',
                               choose=choose,force=force)
            eigenvalues = loadtxt(fname)
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading components from database\n',end='')
            fname = check_load('_'.join(fname.split('_')[:-1])+\
                               '_components.csv',choose=choose,force=force)
            components = transpose(loadtxt(fname,delimiter=','))
            self.__pca = components
            print(verbose*f'>>> {fname}\n',end='')
            T = results[:,0] # retrieve T and projections
            projections = results[:,1:]
        if plot: # plotting
            default_title = f'{self.name}' if self.name else \
                            f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)} PCA'\
                            if short(selection_name) else ' PCA'
            title = set_optional(args,'title',default_title)
            title+= f', first {DEF_EIG} eigenvalues'
            figure()
            plt.grid(zorder=0)
            plt.xlim(0,DEF_EIG)
            plt.bar(arange(DEF_EIG)+0.5,eigenvalues[0:DEF_EIG],width=1,zorder=3)
            plt.xticks(arange(DEF_EIG)+0.5,[f'{i+1}' for i in range(DEF_EIG)])
            plt.ylabel('eigenvalue')
            plt.title(title)
            draw()
            pause()
            print(verbose*f'plotted first {DEF_EIG} eigenvalues; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = fname.split('_components')[0].split('.pdf')[0] if fname\
                        else f'{self.fname(output_name)}{suffix}'
                fname = check_save(fname+\
                                   '_eigenvalues.pdf',choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            default_title = f'{self.name}' if self.name else \
                    f'{self.input_name.split("/")[-1]}'
            default_title+= f' {short(selection_name)} PCA'\
                            if short(selection_name) else ' PCA'
            title = set_optional(args,'title',default_title)
            for h in range(0,n_pca-1):
                for k in range(h+1,n_pca):
                    fname = projection_plot(h,k,fname=fname,title=title)
            if n_pca>=3:
                projection_plot(0,1,2,fname,title)
        if output:
            return results,eigenvalues,components
            


