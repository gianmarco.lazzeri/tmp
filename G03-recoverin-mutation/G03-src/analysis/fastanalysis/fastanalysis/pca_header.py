    def pca(self,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
        # parameters
        fname  = ''
        suffix = '_pca'
        # auxiliary function
        def projection_plot(h,k,z=None,fname='',title=''): # plot proj. h vs k
            title+=f', components {h+1} vs {k+1}'
            gs=GridSpec(1,2)
            figure(figsize=(8.533,4.8))
            col=colors(len(T),DEF_CMAP2)
            if not z:
                a1=plt.subplot(gs[0])
                a2=plt.subplot(gs[1]) # barplot
                [a1.plot(projections[i,h],projections[i,k],'.',color=col[i])\
                for i in tqdm(range(len(T)),disable=not verbose)]
            else:
                title += f' vs {z+1}'
                a1=plt.subplot(gs[0],projection='3d')
                a2=plt.subplot(gs[1]) # barplot
                [a1.plot([projections[i,h]],[projections[i,k]],\
                         [projections[i,z]],'.',color=col[i])\
                for i in tqdm(range(len(T)),disable=not verbose)]  
                a1.set_zlabel(f'component {z+1}')
            a1.grid()
            a1.set_xlabel(f'component {h+1}')
            a1.set_ylabel(f'component {k+1}')
            a1.set_title(title)
            img = a2.imshow([[0,0]],cmap=DEF_CMAP2,
                            vmin=T.min(),vmax=round(T.max()+self.dt))
            plt.gca().set_visible(False)
            cb=plt.colorbar(img,ax=a2,ticks=[i for i in range(0,400,100)])
            cb.ax.set_yticklabels([f'{i} ns' for i in range(0,400,100)])
            a1.set_position([0.11,0.12,0.72,0.75])
            a2.set_position([0.72,0.16,0.90,0.75])
            plt.grid()
            plt.title(title)
            draw()
            pause()
            print(verbose*f'plotted PCA component 1 vs 2; ',end='')
            print(verbose*f'handle with "figure({gcf().number})"\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = '_'.join(fname.split('_')[:-1])+f'_{h+1}{k+1}'
                fname+= f'{z+1}.pdf' if z else '.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            return fname

