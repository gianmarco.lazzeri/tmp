        # load optional parameters
        input_name  = set_optional(args,'input_name',  self.input_name)
        input_folder= set_optional(args,'input_folder',self.input_folder)
        output_name = set_optional(args,'output_name', self.output_name)
        input_traj  = args.get('input_traj')\
              if type(args.get('input_traj')) is list or\
                 type(args.get('input_traj')) is str else ''
        selection_name = set_optional(args,'selection_name',self.selection_name)
        selection_name = selection_name if selection_name else 'all'
        reference_name = set_optional(args,'reference_name',self.reference_name)
        reference_frame= set_optional(args,'reference_frame',
                                       self.reference_frame)
        start = set_optional(args,'start',self.start)
        step  = set_optional(args,'step', self.step)
        stop  = set_optional(args,'stop', self.stop)
        n     = int(set_optional(args,'n_avg',0)/2)
        cut   = set_optional(args,'cut',1000.)
        n_pca = max(0,set_optional(args,'n_pca',DEF_PCA))
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        align   = set_optional(args,'align',  True) # align to reference data
        output  = set_optional(args,'output', True)
        show_max_var = set_optional(args,'show_max_var',True)
        axis = set_optional(args,'axis','xyz') #
        dt = set_optional(args,'dt',self.dt) #
