class system:
    """
    Main class.
    """
    def __init__(self,input_name,**args):
        """
        Parameters
        ----------
        input_name : str
            .pdb or .gro file. Extension is not necessary if the file is .pdb.
            Anyway, mistaken or not, it can always be corrected.
        input_traj : str, optional
            Either a .dcd file or a list of .dcd file. In the first case,
            mistaken or not, it can always be corrected.
        input_folder : str, optional
            A shortcut to avoid lengthy "input_name", "input_traj" and
            "output_name" definitions.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself shall be taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            It is worth only when "reference_name" is not specified, so that the
            "input_name" universe itself shall be taken as reference.
        start : int, optional
            The frame to start with if loading the "input_name" universe.
        step : int, optional
            The frame interval if loading the "input_name" universe.
        stop : int, optional
            The frame to end at if loading the "input_name" universe.
        name : str, optional
            Fancy name which defines the system. If not specified, "input_name"
            shall be chosen instead.
        """
        # parameters
        self.input_name      = str(input_name)
        self.name            = set_optional(args,'name','')
        self.input_traj      = set_optional(args,'input_traj','')
        self.input_folder    = set_optional(args,'input_folder','')
        self.output_name     = set_optional(args,'output_name',input_name)
        self.selection_name  = set_optional(args,'selection_name','')
        self.reference_name  = set_optional(args,'reference_name','')
        self.reference_frame = set_optional(args,'reference_frame',DEF_REF)
        self.start           = set_optional(args,'start',0)
        self.step            = set_optional(args,'step',+1)
        self.stop            = set_optional(args,'stop',-1)
        # universe
        self.__backup  = None # backup copy (useful when changing frames etc.)
        self.universe  = None
        self.reference = None
        self.universe_selection  = None
        self.reference_selection = None  
        self.dt = DEF_DT # default timesteps between frames (ps)
        self.__aligned = False
        # analysis results
        self.__pyff = None
        self.__gyr  = None
        self.__pca  = None
        self.__rmsd = None
        self.__rmsf = None # output of rmsf
        self.__dist = None
        
    def __str__(self):
        if self.universe is None:
            return 'empty universe'
        return   'universe with: '+\
              f'{self.universe.trajectory.n_frames} frames'+\
              f'\n        atoms: '+\
              f'{self.universe.select_atoms("all").n_atoms}'+\
              f'\n      protein: '+\
              f'{self.universe.select_atoms(ALIAS["protein"]).n_atoms}'+\
              f'\n     membrane: '+\
              f'{self.universe.select_atoms(ALIAS["membrane"]).n_atoms}'+\
              f'\n      calcium: '+\
              f'{self.universe.select_atoms(ALIAS["calcium"]).n_atoms}'+\
              f'\n        water: '+\
              f'{self.universe.select_atoms(ALIAS["water"]).n_atoms}'
              
    def pyff_compare(self,matrix,verbose=True):
        """
        If pyfferaph matrix is computed: see the difference with an other
        matrix.
        """
        if self.__pyff is None:
            return
        bigger1,bigger2=matrix_difference(self.__pyff,matrix)
        for i,j in bigger1:
            print(verbose*f'interaction '+\
                  f'{self.universe_selection.residues[i]}'+\
                  f' - {self.universe_selection.residues[j]} found in '+\
                  f'system matrix and not in other\n',end='')
        for i,j in bigger2:
            print(verbose*f'interaction '+\
                  f'{self.universe_selection.residues[i]}'+\
                  f' - {self.universe_selection.residues[j]} found in '+\
                  f'other matrix and not in system\n',end='')
        return len(bigger1)+len(bigger2)
        
    def pca_project(self,selection,n_pca=DEF_PCA):
        """
        Returns projections according to stored pca.
        """
        if self.__pca is None:
            return
        n_frames = selection.universe.trajectory.n_frames
        projection=zeros((n_frames,n_pca))
        mean_pos = zeros(len(self.__pca)) # mean position
        for frame in selection.universe.trajectory:
            mean_pos += selection.positions.ravel()
        mean_pos *= 1/n_frames
        for i,frame in enumerate(selection.universe.trajectory):
            pos = selection.positions.ravel()-mean_pos
            projection[i,:] = dot(pos,self.__pca[:,:n_pca])
        T = array([[self.dt*i] for i in range(len(projections))])
        results = concatenate((T,projections),axis=1)
        return results
