        if compute: # computing
            if self.__backup:
                self.load(False,**args)
                print(verbose*'pyfferaph for selection ',end='')
                print(verbose*f'"{selection_name}"\n',end='')
                sbmatrix = _sbmatrix(self.universe,
                                     self.universe_selection,verbose=verbose)
                hcmatrix = _hcmatrix(self.universe,
                                     self.universe_selection,verbose=verbose)
                hbmatrix = _hbmatrix(self.universe,
                                     self.universe_selection,verbose=verbose)
                print(verbose*'retrieving segments list\n',end='')
                segments =\
                [residue.segid for residue in self.universe_selection.residues]
                if save: # saving
                    print(verbose*'saving salt bridges matrix to file\n',end='')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                       '_sbmatrix.csv',choose=choose,force=force)
                    savetxt(fname,transpose(sbmatrix),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving hydrophobic contacts to file\n',end='')
                    fname = check_save(fname.split('_sbmatrix.csv')[0]+\
                                       '_hcmatrix.csv',choose=choose,force=force)
                    savetxt(fname,transpose(hcmatrix),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving hydrogen bonds matrix to file\n',end='')
                    fname = check_save(fname.split('_hcmatrix.csv')[0]+\
                                       '_hbmatrix.csv',choose=choose,force=force)
                    savetxt(fname,transpose(hbmatrix),delimiter=',')
                    print(verbose*f'>>> {fname}\n',end='')
                    print(verbose*'saving segments dictionary to file\n',end='')
                    fname = check_save(fname.split('_hbmatrix.csv')[0]+\
                                       '_segments.dic',choose=choose,force=force)
                    savetxt(fname,segments,fmt='%s')
                    print(verbose*f'>>> {fname}\n',end='')
            elif not force: # can't compute
                print('warning: no system loaded!')
                choice = input('loading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return [[]]
            else: # if force option: can't but exit
                print(verbose*'warning: no system loaded!\n',end='')
                if output:
                    return [[]]
                else:
                    return
        if not compute: # loading
            print(verbose*'reading salt bridges from database\n',end='')
            fname = check_load(f'{self.fname(output_name,selection_name)}'+\
                               f'{suffix}_sbmatrix.csv',choose=choose,force=force)
            sbmatrix = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading hydrophobic contacs from database\n',end='')
            fname = check_load(fname.split('_sbmatrix.csv')[0]+\
                               f'_hcmatrix.csv',choose=choose,force=force)
            hcmatrix = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading hydrogen bonds from database\n',end='')
            fname = check_load(fname.split('_hcmatrix.csv')[0]+\
                               f'_hbmatrix.csv',choose=choose,force=force)
            hbmatrix = loadtxt(fname,delimiter=',')
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*'reading segments dictionary\n',end='')
            fname = check_load(fname.split('_hbmatrix.csv')[0]+\
                               f'_segments.dic',choose=choose,force=force)
            with open(fname) as f:
                segments = [x.replace('\n','') for x in f.readlines()]
            print(verbose*f'>>> {fname}\n',end='')
        default_title = f'{self.name}' if self.name else \
                        f'{self.input_name.split("/")[-1]}'
        default_title+= f' {short(selection_name)}'\
                        if short(selection_name) else ''
        title = set_optional(args,'title',default_title)
        if plot: # plotting
            fname = '_'.join(fname.split('_')[:-1])+f'_sbmatrix.pdf'
            fname = matplot(sbmatrix,fname,
                            title+', salt bridges interaction matrix')
            fname = '_'.join(fname.split('_')[:-1])+f'_hcmatrix.pdf'
            fname = matplot(hcmatrix,fname,
                            title+', hydrophobic contacts interaction matrix')
            fname = '_'.join(fname.split('_')[:-1])+f'_hbmatrix.pdf'
            fname = matplot(hbmatrix,fname,
                            title+', hydrogen bonds interaction matrix')
        fname='_'.join(fname.split('_')[:-1])
        matrix = filter_threshold(sbmatrix,fname+'_sbfilter.pdf',
                                           title+', salt bridges')
        matrix+= filter_threshold(hcmatrix,fname+'_hcfilter.pdf',
                                           title+', hydrophobic contacts')
        matrix+= filter_threshold(hbmatrix,fname+'_hbfilter.pdf',
                                           title+', hydrogen bonds')
        matrix = matrix>0
        if plot:
            fname = matplot(matrix,fname+'.pdf',
                            title+', full interaction matrix',cbar=False)
        self.__pyff = matrix
        if output:
            return matrix
