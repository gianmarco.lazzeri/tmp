    def load(self,save=False,**args):
        """
        Load the "input_name" universe, the "reference_name" universe and the
        corresponding selections. Do nothing if it would change nothing.
        
        Parameters
        ----------
        save : bool, default is False
            If True: save universe selection .pdb and .dcd to file.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start : int, optional
            The frame to start with when loading the "input_name" universe.
        step : int, optional
            The frame interval when loading the "input_name" universe.
        stop : int, optional
            The frame to end at when loading the "input_name" universe.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is False, False.
        verbose : bool, optional
            Default is False.
        """
        # load optional parameters
        output_name      = args.get('output_name')\
                        if args.get('output_name')\
                        else self.output_name
        selection_name   = args.get('selection_name')\
                        if args.get('selection_name')\
                        else self.__log['selection_name']
        reference_name   = args.get('reference_name')\
                        if type(args.get('reference_name')) is str\
                        else self.__log['reference_name']
        reference_frame  = args.get('reference_frame')\
                        if args.get('reference_frame')\
                        else self.__log['reference_frame']
        start   = args.get('start')   if args.get('start')\
                        else self.__log['start']
        step    = args.get('step')    if args.get('step')\
                        else self.__log['step']
        stop    = args.get('stop')    if args.get('stop')\
                        else self.__log['stop']
        choose  = args.get('choose')  if args.get('choose')  else False
        force   = args.get('force')   if args.get('force')   else False
        verbose = args.get('verbose') if args.get('verbose') else False
        # create a backup if it doesn't already exist 
        if not self.__backup:
            if verbose:
                print(f'loading main universe data')
            pdb = (self.input_folder!='')*f'{self.input_folder}/'\
                  +self.input_name
            pdb = pdb if pdb[-4:]=='.pdb' or pdb[-4:]=='.gro' else pdb+'.pdb'
            pdb = check_load(pdb,choose=choose,force=force)
            if not self.input_traj:
                dcd = '.'.join(pdb.split('.')[:-1]) + '.dcd'
                dcd = check_load(dcd,choose=choose,force=force)
            elif type(self.input_traj) is str:
                dcd = (self.input_folder!='')*f'{self.input_folder}/'\
                      +self.input_traj
                dcd = dcd if dcd[-4:]=='.dcd' else dcd+'.dcd'
                dcd = check_load(dcd,choose=choose,force=force)
            else:
                dcd = [(self.input_folder!='')*f'{self.input_folder}/'\
                       +dcd for dcd in self.input_traj]
            if verbose:
                print(f'>>> {pdb}\n>>> {dcd}')
            self.__backup     = Universe(pdb,dcd)
            # updating input infos
            self.input_name   = pdb.split('/')[-1]
            self.input_folder =  '/'.join(pdb.split('/')[0:-1])
            self.input_traj   = ['/'.join(dcd.split('/')[0:-1])\
                                 for dcd in dcd] if type(dcd) is list else\
                                 '/'.join(dcd.split('/')[0:-1])
        # load universe if needed
        if not (self.__log['start'] == start\
           and  self.__log['step']  == step\
           and  self.__log['stop']  == stop)\
        or not self.universe:
            self.universe  = self.__backup.copy()
            stop = stop  if stop > 0 else self.__backup.trajectory.n_frames
            if verbose:
                print(f'extracting {int((stop-start)/step)} frames')
            self.universe.transfer_to_memory(start=start,stop=stop,step=step,\
                                             verbose=verbose)
            self.dt = self.universe.trajectory.dt/1e3
        # load reference if needed
        if not self.__log['reference_name'] == reference_name\
        or not self.reference:
            if verbose:
                print(f'loading reference data')
            if reference_name:
                pdb = (self.input_folder!='')*f'{self.input_folder}/'\
                      +reference_name
                pdb = pdb if pdb[-4:]=='.pdb'or pdb[-4:]=='.gro' else pdb+'.pdb'
                pdb = check_load(pdb,choose=choose,force=force)
                if verbose:
                    print(f'>>> {pdb}')
                self.reference = Universe(pdb)
            else:
                self.reference = self.__backup.copy()
        # load ref frame only when the main universe itself is taken as ref 
        if not self.__log['reference_frame'] == reference_frame\
       and not reference_name:
            if verbose:
                print(f'extracting reference frame')
            self.reference.transfer_to_memory(start=reference_frame,
                                              stop =reference_frame+1)
        # always load selections
        if verbose:
            print(f'loading selection "{selection_name}"')
        real_selection_name = ALIAS[selection_name] if selection_name in ALIAS\
                              else selection_name
        self.universe_selection  =\
            self.universe.select_atoms(real_selection_name)
        self.reference_selection =\
            self.reference.select_atoms(real_selection_name)
        # update all infos and parameters
        self.__log['start'] = start
        self.__log['stop']  = stop
        self.__log['step']  = step
        self.__log['selection_name']  = selection_name
        self.__log['reference_name']  = reference_name
        self.__log['reference_frame'] = reference_frame
        # save
        if save:
            if verbose:
                print(f'extracting .pdb of selection "{selection_name}"') 
            fname = check_save(f'{self.fname(output_name)}.pdb',
                               choose=choose,force=force)
            self.universe_selection.write(fname)
            if verbose:
                print(f'>>> {fname}')
                print(f'extracting {self.universe.trajectory.n_frames} '+\
                      f'frames .dcd of selection "{selection_name}"')
            fname = '.'.join(fname.split('.')[:-1])
            fname = check_save(f'{fname}.dcd',choose=choose,force=force)
            with Writer(fname,self.universe_selection.n_atoms) as W:    
                for frame in self.universe.trajectory:
                    W.write(self.universe_selection)
            if verbose:
                print(f'>>> {fname}')
