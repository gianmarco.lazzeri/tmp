    def gyr(self,compute=True,save=True,plot=True,**args):
        """
        Return the gyration radius of "self.universe_selection".

        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute gyration radius.
            If True and system data are not loaded: ask wheter to load a .csv.
            If False: load a .csv.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        verbose : bool, optional
            Default is False.
        output : bool, optional
            If False: don't return anything. Default is True.

        Returns
        -------
        T,results : tuple
            "T" is the time vector, "results" is the gyration radius vector.
        """
        # parameters
        fname  = ''
        suffix = '_gyr'
