    def dist(self,sel,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        sel : str
            Selection name (to be aligned with self.universe_selection).
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
        # parameters
        fname  = ''
        suffix = '_dist'
