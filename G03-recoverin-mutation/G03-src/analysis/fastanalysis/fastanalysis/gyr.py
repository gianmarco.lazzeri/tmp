    def gyr(self,compute=True,save=True,plot=True,**args):
        """
        Return the gyration radius of "self.universe_selection".

        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute gyration radius.
            If True and system data are not loaded: ask wheter to load a .csv.
            If False: load a .csv.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        verbose : bool, optional
            Default is False.
        output : bool, optional
            If False: don't return anything. Default is True.

        Returns
        -------
        T,results : tuple
            "T" is the time vector, "results" is the gyration radius vector.
        """
        # parameters
        fname  = ''
        suffix = '_rmsd'
        # optional parameters
        output_name = set_optional(args,'output_name',self.output_name)
        selection_name = set_optional(args,'selection_name',
                                self.__log['selection_name'])
        start = set_optional(args,'start',self.__log['start'])
        step  = set_optional(args,'step', self.__log['step'])
        stop  = set_optional(args,'stop', self.__log['stop'])
        n = set_optional(args,'n',0)
        choose  = set_optional(args,'choose', True)
        force   = set_optional(args,'force',  False)
        verbose = set_optional(args,'verbose',False)
        output  = set_optional(args,'output', True)
        # getting T,results
        if compute: # computing
            if self.__backup:
                self.load(False,start=start,stop=stop,step=step,
                          selection_name=selection_name)
                if verbose:
                    print('computing gyration radius '+\
                    f'for selection "{selection_name}"')
                results = [self.universe_selection.radius_of_gyration()\
                for frame in tqdm(self.universe.trajectory,disable=not verbose)]
                dt = self.dt
                T  = [x*dt for x in range(len(results))]
                if save: # saving
                    if verbose:
                        print('saving data to file')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    if verbose:
                        print(f'>>> {fname}')
            elif not force: # can't compute
                choice = input('warning: no system loaded!'\
                            +'\nloading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return []
            else: # if force option: can't but exit
                if verbose:
                    print('warning: no system loaded!')
                if output:
                    return []
        if not compute: # loading
            if verbose:
                print('reading data from database')
            fname = check_load(f'{self.fname(output_name)}{suffix}.csv',\
                               choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            if verbose:
                print(f'>>> {fname}')
        if plot: # plotting
            title = args.get('title') if args.get('title') else\
            f'{self.name} {self.short(selection_name)}, radius of gyration'\
            if self.name else f'{self.input_name.split("/")[-1]} '+\
                 f'{self.short(selection_name)}, radius of gyration'
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{n*dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            draw()
            pause()
            if verbose:
                print(f'plotted gyration radius; handle with'+\
                      f'figure({gcf().number})')
            if save:
                if verbose:
                    print('saving plot to file')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                if verbose:
                    print(f'>>> {fname}')
        if output:
            return T,results
