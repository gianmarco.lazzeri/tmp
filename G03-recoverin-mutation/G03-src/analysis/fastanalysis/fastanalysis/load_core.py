        # create a backup universe if it doesn't already exist
        if self.__backup is None:
            print(verbose*'loading main universe data\n',end='')
            pdb = self.fname(input_name,'')+'.pdb'
            pdb = check_load(pdb,choose=choose,force=force)
            if not self.input_traj:
                dcd = '.'.join(pdb.split('.')[:-1]) + '.dcd'
                dcd = check_load(dcd,choose=choose,force=force)
            elif type(self.input_traj) is str:
                dcd = self.fname(input_traj,'')+'.dcd'
                dcd = check_load(dcd,choose=choose,force=force)
            else:
                dcd = [self.fname(dcd,'')+'.dcd' for dcd in self.input_traj]
            print(verbose*f'>>> {pdb}\n>>> {dcd}\n',end='')
            self.__backup = Universe(pdb,dcd) # backup universe
            self.input_name   = pdb.split('/')[-1] # update infos
            self.input_folder =  '/'.join(pdb.split('/')[0:-1])
            self.input_traj   = ['/'.join(dcd.split('/')[0:-1])\
                                 for dcd in dcd] if type(dcd) is list else\
                                 '/'.join(dcd.split('/')[0:-1])
        # load universe if needed
        if not (self.start==start and self.step==step and self.stop==stop)\
        or not self.universe:
            self.universe  = self.__backup.copy()
            self.start = start # update parameters
            self.stop = stop
            self.step = step
            stop = stop  if stop > 0 else self.__backup.trajectory.n_frames
            print(verbose*f'loading {int((stop-start)/step)} frames\n',end='')
            self.universe.transfer_to_memory(start=start,step=step,stop=stop,\
                                             verbose=verbose)
            self.dt = self.universe.trajectory.dt/1e2
        # load reference if needed
        if not self.reference_name==reference_name or not self.reference:
            print(verbose*'loading reference data\n',end='')
            if reference_name:
                pdb = self.fname(reference_name,'')+'.pdb'
                pdb = pdb if pdb[-4:]=='.pdb'or pdb[-4:]=='.gro' else pdb+'.pdb'
                pdb = check_load(pdb,choose=choose,force=force)
                print(verbose*f'>>> {pdb}\n',end='')
                self.reference = Universe(pdb)
                self.__aligned = False # need to align if requested
            self.reference_name = reference_name # update parameter
        # load ref frame only when the main universe itself is taken as ref 
        if (not self.reference_frame==reference_frame and not reference_name)\
         or not self.reference:
            self.__aligned = False # need to align if requested
            self.reference = self.__backup.copy()
            self.reference_frame = reference_frame # update parameter
            reference_frame = reference_frame if reference_frame >= 0 else\
                              self.reference.trajectory.n_frames-1
            print(verbose*'extracting reference frame ',end='')
            print(verbose*f'{reference_frame}\n',end='')
            self.reference.transfer_to_memory(start=reference_frame,
                                              stop =reference_frame+1)
        # always load selections
        if not self.selection_name == selection_name:
            print(verbose*f'loading selection "{selection_name}"\n',end='')
        sel = real_selection_name(selection_name)
        self.universe_selection  = self.universe.select_atoms(sel)
        self.reference_selection = self.reference.select_atoms(sel)
        self.selection_name = selection_name # update parameter
        if align and not self.__aligned:
            print(verbose*f'aligning system to reference\n',end='')
            self.__aligned = True
            AlignTraj(self.universe,self.reference,select=sel,
                      weights='mass',in_memory=True,verbose=verbose).run()
        # save pdb/dcd
        if save:
            print(verbose*'extracting .pdb of selection ',end='')
            print(verbose*f'"{selection_name}"\n',end='')
            fname = check_save(f'{self.fname(output_name)}.pdb',
                               choose=choose,force=force)
            self.universe_selection.write(fname)
            print(verbose*f'>>> {fname}\n',end='')
            print(verbose*f'extracting {self.universe.trajectory.n_frames} '+\
                  verbose*f'frames of selection "{selection_name}"\n',end='')
            fname = '.'.join(fname.split('.')[:-1])
            fname = check_save(f'{fname}.dcd',choose=choose,force=force)
            with Writer(fname,self.universe_selection.n_atoms) as W:    
                for frame in self.universe.trajectory:
                    W.write(self.universe_selection)
            print(verbose*f'>>> {fname}\n',end='')
