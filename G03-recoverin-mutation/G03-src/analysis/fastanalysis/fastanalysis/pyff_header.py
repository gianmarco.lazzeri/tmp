    def pyff(self,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
        # parameters
        fname  = ''
        suffix = '_pyff'
        # auxiliary functions
        def matplot(matrix,fname='',title='',cbar=True):
            figure()
            ax = plt.axes()
            for i,segment in enumerate(set(segments)):
                col = plt.get_cmap('Set1')(i)
                for j in range(len(segments)):
                    if segments[j]==segment:
                        ax.add_patch(patches.Rectangle((j,0),1,len(matrix),lw=0,
                        color=col,alpha=0.2))
                        ax.add_patch(patches.Rectangle((0,j),len(matrix),1,lw=0,
                        color=col,alpha=0.2))
                ax.add_patch(patches.Rectangle((-1,-1),1,1,lw=0,
                        color=col,alpha=0.2,label=segment))
            heatmap(matrix,square=True,cmap='binary',cbar=cbar)
            plt.xlabel('residues')
            plt.ylabel('residues')
            plt.xticks([])
            plt.yticks([])
            plt.subplots_adjust(top=0.92)
            plt.suptitle(title)
            if i>0:
                pos = ax.get_position()
                pos.x0 += 0.1
                ax.set_position(pos)
                plt.legend(bbox_to_anchor=(-0.067,1))
            draw()
            pause()
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            return fname
        def filter_threshold(matrix,fname,title):
            print(verbose*'filtering interactions\n',end='')
            edges_size = []
            maxclustsize = [] # size of biggest cluster according to thresh
            for val in DEF_VALS:       
                boolmat = matrix>val
                G=Graph(boolmat)
                maxclustsize.append(len(max(connected_components(G),key=len)))
                edges_size.append(len(G.edges))
            fit = curve_fit(sigmoid,DEF_VALS,maxclustsize,p0=(0.5,50,50,1),
                            maxfev=1000000)
            flex = fit[0][0]
            if flex > min(DEF_VALS) and flex < max(DEF_VALS):
                print(f'> found flex at: {flex:.3f}')
            else:
                print('found negative flex: '+\
                'looking for max gap in maxclustsize instead')
                flex = DEF_VALS[argmax(-diff(maxclustsize))+1]
                print(f'> found critical value at: {flex:.3f}')
            figure()
            plt.grid()
            plt.plot(DEF_VALS,maxclustsize,'.',label='max clust size')
            plt.plot(DEF_VALS,edges_size,dashes=[2,2],label='n of edges')
            d = max(edges_size)-min(maxclustsize)
            plt.xlim(min(DEF_VALS),max(DEF_VALS))
            plt.ylim(min(maxclustsize)-d*.1,max(edges_size)+d*.3)
            plt.xlabel("$p_{min}$")
            plt.ylabel("size")
            x = linspace(min(DEF_VALS),max(DEF_VALS))
            plt.plot(x,sigmoid(x,*fit[0]),label='fit')
            plt.title(title)
            draw()
            pause()
            while True:
                try:
                    choice = input('> choose your threshold'+\
                                   '(enter to confirm): ')
                    flex = float(choice) if len(choice) else flex
                    i = len([1 for val in DEF_VALS if val<flex])
                    break
                except:
                    pass
            plt.plot(flex,sigmoid(flex,*fit[0]),'o',\
                     label=f'threshold ({flex:.2f},{sigmoid(flex,*fit[0]):.2f})', 
                     color='red')
            plt.title(title+f': filtered {edges_size[i]} contacts')
            plt.legend()
            print(verbose*'plotted fit of persistence threshold\n',end='')
            if save:
                print(verbose*'saving plot to file\n',end='')
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                print(verbose*f'>>> {fname}\n',end='')
            boolmat = matrix>flex
            return boolmat
