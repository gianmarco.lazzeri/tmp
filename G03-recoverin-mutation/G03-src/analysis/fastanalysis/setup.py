from setuptools import setup

setup(name='fastanalysis',
      version='0.1',
      description='Fast analysis of md results',
      install_requires=['numpy>=1.18.0',
                        'matplotlib>=3.1.0',
			'MDAnalysis>=0.20.0',
                        'tqdm>=4.41.0',
                        'scipy>=1.4.0',
                        'seaborn>=0.9.0',
                        'networkx>=2.0'],
      author='Gianmarco Lazzeri',
      author_email='gianmarco.lazzeri@studenti.unitn.it',
      license='MIT',
      packages=['fastanalysis'],
      zip_safe=False)
