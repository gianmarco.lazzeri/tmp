# general code
from fastanalysis import *
s=system('rec_r_protein',
input_folder='../cbp/G03-recoverin-mutation/G03-data/02-analysis',
name='Wild type protein',
step=1,reference_name='',selection_name='backbone')
# load
s.load(verbose=True,force=True) # load frames, no lost time
s.pca(verbose=True,force=True)
s.rmsd(verbose=True,selection_name='')
s.rmsf(verbose=True,selection_name='all')
matrix = s.pyff(verbose=True)


# nella cartella di analisi del cluster, dove ci sono già
# rec_r_waterfree.pdb/dcd rec_r_protein.pdb/dcd
# rec_r_mutated_waterfree.pdb/dcd rec_r_mutated_protein.pdb/dcd
from fastanalysis import *
# wild type
s1 = system(name='Wild type protein',
            input_name='rec_r_waterfree',output_name='rec_r')
s1.load(verbose=True,force=True,align=False)
# pyfferaph & dist were missing
s1.dist('membrane',axis='z',verbose=True,choose=False,output=False)
matrix = s1.pyff(verbose=True,step=30)
# mutated
s2 = system(name='Mutated protein',
            input_name='rec_r_mutated_waterfree',output_name='rec_r_mutated')
s2.load(verbose=True,force=True,align=False)
# on cluster
s1.pca(force=True,selection_name='name CA',plot=False,verbose=True,output=False)
s2.load(selection_name='name CA')
s1.pca_project(s2.universe_selection).\
tofile('rec_r_mutated_pc_of_non_mutated.csv')
s2.pca(force=True,selection_name='name CA',plot=False,verbose=True,output=False)
s2.gyr(force=True,plot=False,output=False,selection_name='protein',verbose=True)
s2.rmsd(force=True,plot=False,output=False,verbose=True)
s2.rmsf(force=True,plot=False,output=False,verbose=True)
s2.dist('membrane',axis='z',verbose=True,choose=False,output=False)
s2.pyff(verbose=True,step=30,plot=False,output=False)
# comparisons - do at home
s2.pyff_compare(matrix)

# logs - do on cluster
s1.log(input_folder='../01-raw/05-membrane',force=True,n_avg=1000)
s2.log(input_folder='../01-raw/04-mutated-membrane',force=True,n_avg=1000)
