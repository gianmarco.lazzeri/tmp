#!/usr/bin/env python
# coding: utf-8

# In[89]:


#This program computes the time correlation of a time series. In our case sample can be the vector of the RMSD, RMSF etc.
import numpy as np
from numpy.linalg import norm
import numpy.linalg as lg
import numpy.random as rnd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from tqdm.notebook import tqdm


# In[92]:


def CorrelationTime(sample,cutoff=0):
    data = np.loadtxt(sample,delimiter=',')
    T= data[:,0]
    results = data[:,1:]
    avgSam = np.mean(results,axis=0)	#mean value
    sigmaSam = 0
    for x in results:
        sigmaSam += sum([norm(x-avgSam[i])**2 for i in range(len(avgSam))])
    sigmaSam = (sigmaSam/len(results))**(1/2)
    n=len(T)
    m=min(len(T),cutoff) if cutoff else len(T)
    autoCorr = np.zeros(m)
    for k in range(m):
        if k%100==0:
            print(f'{k} out of {m}')
        N = min(m,n-k)
        autoCorr[k] = 1/(N*sigmaSam**2) * sum([(results[i]-avgSam).dot(results[i+k]-avgSam) for i in range(N)])
    T = T[0:m]
    plt.plot(T, autoCorr)
    plt.xlabel('timestep')
    plt.ylabel('autocorrelation')
    plt.grid()
    params, pcov = curve_fit(func, T, autoCorr,p0=[1,1]) ### curve_fit takes data and tries to fit 'func'
    xdata = np.arange(min(T),max(T),(max(T)-min(T))/1000)
    plt.plot(xdata, func(xdata, *params), 'r-')
    print(f'params {params}')
    plt.draw()
    plt.pause(0.01)
    tau = 1/params[1]
    dtau = pcov[1][1]**(1/2)
    print(f'tau = {tau:4.3} +- {dtau:4.3}')
    Nindipframe = int(round(n/tau)) #number of independent frames
    print(f'n of independent frames {Nindipframe}')
    SE = (np.sqrt(sigmaSam))/(np.sqrt(Nindipframe-1))
    return tau,SE
    
    
    


# In[93]:


CorrelationTime("rec_r_gyr.csv")

