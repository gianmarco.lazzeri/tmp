# pyfferaph.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i pyfferaph.py <mol> -d <dcd> -i <input> -o <output> -s <selection> --start <start> --stop <stop> --step <step> --handle -f -c
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'all'
# --filter -> choose persistance threshold value by yourself
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# --handle -> don't automatically save figures, do it by yourself
# EXAMPLE
# python -i pyfferaph.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein --step 5
# TASK
# build interaction matrices 'à la pyinteraph' (hc, sb, hb)
# filter persistent contacts and build a whole matrix
# plot results
# save network to file

# PARAMETERS

N_sel = 16 # in writing output file
sel_default = 'all' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files
N_vals=100 # n of values to choose persistence from
hc_file=''
sb_file=''
hb_file=''
# to build the matrix, define all atoms you want to consider
atoms_for_matrix = 'resname GLYM or protein or resname CAL or resname DGPS DGPC DGPE'
# salt bridge
sel_basic = "(resname LYS and name NZ HZ*) or (resname ARG and name CZ NH* 1HH* 2HH*) or resname CAL"
sel_acidic = "(resname ASP and name CG OD*) or (resname GLU and name CD OE*) or (resname DGPS and name C13 O13A O13B)"
# hydrophobic
backbone = "name N HN CA HA C O OT1 OT2 HT2"
hydrophobicResidues = "resname ALA ILE VAL LEU PHE MET TRP PRO GLYM"
chain1 = "O21 O22 C21 C22 C23 C24 C25 C26 C27 C28 C29 0C21 1C21 2C21 3C21 4C21 5C21 6C21 7C21 8C21 9C21 0C22"
chain2 = "O31 O32 C31 C32 C33 C34 C35 C36 C37 C38 C39 0C31 1C31 2C31 3C31 4C31 5C31 6C31 7C31 8C31 9C31 0C32"
# for h-bonds
sel1 = 'protein or resname GLYM'
sel2 = 'resname GLYM or protein or resname CAL or resname DGPS DGPC DGPE'
lipidic_donors =[]
lipidic_acceptors = ["O12", "O13", "O14", "O11", "O21", "O22", "O31", "O32", "O13A", "O13B"]

# BEGIN

print('*****************************')
print('md pyfferaph analysis tool...')
print('loading libraries...')

# LIBRARIES

import argparse
import MDAnalysis as mda
import MDAnalysis.analysis.hbonds as hb
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.patches as patches
from scipy.spatial.distance import squareform
import seaborn as sns
from itertools import product
import inspect
import pandas as pd
import time
import os 
import glob
from collections import namedtuple
import networkx as nx
from scipy.optimize import curve_fit
from scipy.optimize import fsolve
from scipy.special import expit
from MDAnalysis.analysis.base import analysis_class
from MDAnalysis.lib.distances import capped_distance, self_capped_distance
from MDAnalysis.lib.distances import distance_array, self_distance_array
import multiprocessing 

print('please check selections in the pyfferaph.py file')

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

def SaltBridge(universe, nres, sel_acidic, sel_basic, nframe, sb_file=sb_file, cutoff=4.5):
    # For the membrane we added the carboxylic group of DGPS in analogy to GLU/ASP

    # Selection taken from the pyinteraph paper, different from charged_groups.ini
    # Added calcium ions as charged residue for salt bridges 
    # sel_basic = "(resname LYS and name NZ HZ*) or (resname ARG and name CZ NH* 1HH* 2HH*) or resname CAL"
    # sel_acidic = "(resname ASP and name CG OD*) or (resname GLU and name CD OE*) or (resname DGPS and name C13 O13A O13B)"

    # Histidine must be accounted for when its protonation state gives it a charge, i.e. when both N_epsilon
    # and N_delta have hydrogens.
    
    # reference groups (first frame of the trajectory, but you could also use a
    # separate PDB, eg crystal structure)
    acidic = universe.select_atoms(sel_acidic)
    basic = universe.select_atoms(sel_basic)

    salt_bridge = np.zeros((nres, nres), int)

    for ts in universe.trajectory:
        AA = capped_distance(acidic.positions, basic.positions, max_cutoff=cutoff, return_distances=False, box=acidic.dimensions)
        # At least for salt bridges, we need only that one bridge is formed. 
        # Therefore we will map the pairs of atoms indices into the pairs of 
        # residues and take the unique pair to compose the matrix.
        # It is symmetric, but we can use the upper diagonal part.
        residxs = []
        for resAcid,resBasic in zip(acidic[AA[:,0]].resindices, basic[AA[:,1]].resindices):
             residxs.append([resAcid,resBasic])
        residxs = np.unique(np.array(residxs), axis=0) # shifting back
        for r in residxs:
            if r[0] in selection.resindices and r[1] in selection.resindices:
                salt_bridge[r[0],r[1]] += 1

    pref = 1.
    SSBB = (salt_bridge+salt_bridge.T)*pref / nframe
    np.savetxt(sb_file,SSBB)
    print('>>>',sb_file)

def HydrophobicInteraction(universe, nres, hcsel, nframe, hc_file=hc_file, cutoff=5.51):
    hcMatrix = np.zeros((nres, nres), int)
    for ts in universe.trajectory:
        hydroDists, _ = self_capped_distance(hcsel.center_of_mass(compound='residues', pbc=True),
                                             max_cutoff=cutoff,
                                             box=selection.dimensions)

        for pair in hydroDists:
            res1 = hcsel.residues[pair[0]].resindex
            res2 = hcsel.residues[pair[1]].resindex
            if res1 in selection.resindices and res2 in selection.resindices:
                hcMatrix[res1, res2] += 1
                if res1 != res2:
                    hcMatrix[res2, res1] += 1

    pref = 1.
    normed_hcMatrix = (hcMatrix)*pref /nframe
    np.savetxt(hc_file, normed_hcMatrix)
    print('>>>',hc_file)

def parallel_hb(start, stop, sel1, sel2, csv_name='hb', up_sel1=False, up_sel2=False, dist=3.5, angle=120.0, accepts=[], dons=[]):

    print('initializing ' + multiprocessing.current_process().name)
    input_hb = hb.HydrogenBondAnalysis(universe, 
                                       sel1, 
                                       sel2, 
                                       update_selection1=up_sel1, 
                                       update_selection2=up_sel2,
                                       distance=dist, angle=angle,
                                       acceptors=accepts,
                                       donors=dons,
                                       pbc=True)
    input_hb.run(start=start, stop=stop)
    hb_csv = csv_name+'_'+str(start)+'_'+str(stop)+'.csv'
    input_hb.generate_table()
    df = pd.DataFrame.from_records(input_hb.table)
    df.to_csv(hb_csv)
    print('>>>',hb_csv)

def defineIntervals(nframe, ncores=0): # 0 for 'all'
    if not ncores:
        print("using all the available cores")
        ncores = multiprocessing.cpu_count()
    traj_per_core = nframe//ncores
    intervals=[]
    for i in range(ncores):
        if i == ncores-1:
            # the None value allows the computation up to the last frame
            intervals.append((i*traj_per_core, None))
        else:
            intervals.append((i*traj_per_core, (i+1)*traj_per_core))
    return intervals

def HBondInteraction(sel1, sel2, nframe, output_name, ncores=0): 
    intervals = defineIntervals(nframe, ncores)
    pool = multiprocessing.Pool()
    for start, stop in intervals:
        pool.apply_async(parallel_hb, args=(start,stop,sel1,sel2), 
                                      kwds={'dist': 3.5, 
                                            'angle': 120.0,
                                            'accepts': lipidic_acceptors,
                                            'dons': lipidic_donors, 
                                            'csv_name': output_name+'_hb'})
    pool.close()
    pool.join()

def MergingHBDataframe(output_name, nframe, ncores=0):
    # merging data frames
    intervals = defineIntervals(nframe, ncores)
    df_list = []
    for start, stop in intervals:
        df_list.append(pd.read_csv(output_name+'_hb_'+str(start)+'_'+str(stop)+'.csv', usecols=['time', 'donor_index', 'acceptor_index']))
    hb_df = pd.concat(df_list, ignore_index=True)
    return hb_df

def HBMatrix(universe, nres, nframe, hb_df, hb_file=hb_file):
    # creating the dictionary to convert atom index into resindex
    index2resindex = {}
    for atom in universe.atoms:
        index2resindex[atom.index] = atom.resindex
    #creating column with donor and acceptor resindices
    hb_df['d_resindex'] = hb_df['donor_index'].map(index2resindex)
    hb_df['a_resindex'] = hb_df['acceptor_index'].map(index2resindex)
    # removing unnecessary columns
    hb_df.drop(['donor_index', 'acceptor_index'], axis=1, inplace=True)
    # creating a string to assess the equivalence of the pairs like 11-12 and 12-11
    hb_df['check_pair'] = hb_df.apply(lambda row: '-'.join(sorted([f'{row["d_resindex"]:.0f}', f'{row["a_resindex"]:.0f}'])), axis=1)
    #removing the same hb at the same time step
    hb_df.drop_duplicates(subset=['time', 'check_pair'], inplace=True)
    #cleaning the dataframe
    hb_df.drop('check_pair', axis=1, inplace=True)

    #creating the matrix
    hbMatrix = np.zeros((nres,nres))

    for index, row in hb_df.iterrows():
        time, d_residx, a_residx = row
        i = int(d_residx)
        j = int(a_residx)
        if i in selection.resindices and j in selection.resindices:
            hbMatrix[i,j] += 1
            if (i!=j):
                hbMatrix[j,i] += 1
    pref = 1.
    fullHB = hbMatrix*pref / nframe
    # Saving matrices:
    np.savetxt(hb_file, fullHB)
    print('>>>',hb_file)

def plot(xxMatrix,name,short_name=''):
    l = len(xxMatrix[0])
    global n_fig
    global filename
    filename = filename.split('.dic')[0].split('.csv')[0].split('.png')[0].split('_hc')[0].split('_sb')[0].split('_hb')[0].split('_matrix')[0].split('_persistence')[0]
    n_fig += 1
    plt.figure(n_fig,figsize=(16,9))
    ax = plt.axes()
    if len(segids.keys()) > 1:
        for i,segid in enumerate(segids):
            for x in segids[segid]:
                if x==segids[segid][0]:
                    ax.add_patch(patches.Rectangle((x,0),1,l,color=plt.get_cmap('Set1')(i),alpha=0.2,lw=0,label=segid))
                else:
                    ax.add_patch(patches.Rectangle((x,0),1,l,color=plt.get_cmap('Set1')(i),alpha=0.2,lw=0))                 
                ax.add_patch(patches.Rectangle((0,x),l,1,color=plt.get_cmap('Set1')(i),alpha=0.2,lw=0))
        sns.heatmap(xxMatrix,square=True,cmap='binary')
        ax.set(xticks=[0,l],yticks=[0,l],xticklabels=[0,l],yticklabels=[0,l])
        pos = ax.get_position()
        pos.x0 += 0.1
        ax.set_position(pos)
        plt.title((sel_name!=sel_default)*f'{sel_name} '+name+' matrix')
        plt.xlabel('residues')
        plt.ylabel('residues')
        plt.legend(bbox_to_anchor=(-0.067,1))
    else:
        sns.heatmap(xxMatrix,square=True)
        ax.set(xticks=[0,l],yticks=[0,l],xticklabels=[0,l],yticklabels=[0,l])
        plt.title((sel_name!=sel_default)*f'{sel_name} '+name+' matrix')
        plt.xlabel('residues')
        plt.ylabel('residues')

    if args.handle==False:
        filename = check(filename+'_'*(len(short_name)>0)+short_name+'_matrix.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

def filter(xxMatrix,name,short_name):
    global n_fig
    global filename
    filename = filename.split('.dic')[0].split('.csv')[0].split('.png')[0].split('_hc')[0].split('_sb')[0].split('_hb')[0].split('_matrix')[0].split('_persistence')[0]
    print(filename)
    print('filtering '+name+' interactions...')
    edges_size = []
    maxclustsize = [] # size of the biggest cluster according to threshold values
    for val in vals:       
        boolmat = [i>val for i in [xxMatrix]]
        G=nx.Graph(boolmat[0])
        maxclustsize.append(len(max(nx.connected_components(G), key=len)))
        edges_size.append(len(G.edges))
    print('fitting optimal persistence threshold...')
    fit = curve_fit(sigmoid, vals, maxclustsize, p0=(0.4,50,50,1), maxfev = 1000000)
    flex = fit[0][0]
    if flex > 0:
        print(f"> found flex at: {flex:.3f}")
    else:
        print('found negative flex: looking for max gap in maxclustsize instead')
        flex = vals[np.argmax(-np.diff(maxclustsize))+1]
        print(f"> found critical value at: {flex:.3f}")
    # closest val
    print('plotting results...')
    n_fig += 1
    plt.figure(n_fig,figsize=(16,9))
    plt.grid()
    plt.plot(vals,maxclustsize, '.',label='max clust size')
    plt.plot(vals,edges_size, dashes=[2,2],label='n of edges')
    plt.xlim((0, 1))
    plt.ylim((min(maxclustsize)*0.95,max(edges_size)*1.05))
    plt.xlabel("$p_{min}$")
    plt.ylabel("size")
    x = np.linspace(max(vals),min(vals))
    plt.plot(x,sigmoid(x,*fit[0]),label='fit')
    if args.filter:
        plt.draw()
        plt.pause(0.001)
        while True:
            try:
                flex = float(input(" choose your optimal threshold value: "))
                break
            except:
                pass
    plt.plot(flex,sigmoid(flex,*fit[0]),'o',label = f'threshold ({flex:.2f},{sigmoid(flex,*fit[0]):.2f})', color = 'red')
    i = 0
    while vals[i]<flex and i < len(vals)-2:
        i += 1
    best_val = vals[i]
    plt.title((sel_name!=sel_default)*f'{sel_name} '+short_name+f' interactions: filtered {edges_size[i]} contacts')
    plt.legend()
    if args.handle==False:
        filename = check(filename+'_persistence_'+short_name+'.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

    boolmat = [i>best_val for i in [xxMatrix]][0]
    print('writing filtered matrix to file...')
    filename = filename.split('.dic')[0].split('.csv')[0].split('.png')[0].split('_hc')[0].split('_sb')[0].split('_hb')[0].split('_matrix')[0].split('_persistence')[0]
    filename = check(filename+'_matrix_'+short_name+'_filter.csv')
    np.savetxt(filename,boolmat)
    print('>>>',filename)
    return boolmat

# auxiliary functions

# Parametric sigmoid function for fitting
def sigmoid(x, x0, k, m, n): 
    y = m / (1 + np.exp(k*(x-x0))) + n
    return y

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description="build interaction matrices 'à la pyinteraph' (hc, sb, hb)\nfilter persistent contacts and build a whole matrix\nplot results and save network to file")
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='is a list of dcd or xtc files: coordinate time series\nif not specified: takes <molecule>.dcd as default')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"is a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
parser.add_argument("--filter",action='store_true',help="choose persistance threshold value by yourself")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
output +='_pyfferaph'
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('output path:        ',output)
print('loading data...')
print('*****************************')

# universe
universe = mda.Universe(mol,dcd)

# statistics
print('   n frames:',universe.trajectory.n_frames)
print('total atoms:',universe.select_atoms("all").n_atoms)
print('    protein:',universe.select_atoms("protein or resname GLYM").n_atoms)
print('   membrane:',universe.select_atoms("resname DGPS DGPC DGPE").n_atoms)
print('    calcium:',universe.select_atoms("resname CAL").n_atoms)
print('      water:',universe.select_atoms("resname TIP3").n_atoms)

# start-step-stop
start = max(0,args.start-1)
step = args.step
stop =(args.stop>=0)*min(args.stop+1,universe.trajectory.n_frames)+(args.stop<0)*universe.trajectory.n_frames
universe.transfer_to_memory(start=start,stop=stop,step=step)

# MAIN PROGRAM

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])

    real_sel_name = sel_name
    # necessary with myrystoil groups
    if sel_name == 'backbone':
        real_sel_name = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
    if sel_name == 'protein':
        real_sel_name = "protein or same segid as protein"
    if sel_name == 'membrane':
        real_sel_name = "resname DGPS DGPC DGPE"

    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    NFRAME = universe.trajectory.n_frames
    selection = universe.select_atoms(f'({atoms_for_matrix}) and {real_sel_name}')
    residxs = np.unique(selection.resindices)
    nres = len(residxs)
    vals = np.arange(0, 1, 1/N_vals) # acceptable persistence threshold values

    print('*****************************')

    i = 0
    segids={}
    for segid in selection.segids:
        segids[segid]=[]

    for residue in selection.residues:
        segids[residue.segid].append(i)
        i+=1

    print('saving segments dictionary to file...')
    filename = check(sel_output+'.dic')
    with open(filename,'w') as f:
        f.writelines(f'{segids}')

    print('*****************************')
    print('running pyfferaph for selection:',sel_name)
    print('selection has',selection.n_atoms,'atoms and',universe.trajectory.n_frames,'frames')

    sb_file = check(filename.split('.dic')[0]+'_matrix_sb.csv')
    SaltBridge(universe, nres, sel_acidic, sel_basic, NFRAME, sb_file=sb_file,cutoff=4.5)

    hc_file = check(sb_file.split('.csv')[0].split('_matrix_sb')[0]+'_matrix_hc.csv')  
    hcSel = universe.select_atoms(f"{hydrophobicResidues} and (not {backbone}) or (resname DGP* and name {chain1} {chain2})")
    HydrophobicInteraction(universe, nres, hcSel, NFRAME,hc_file=hc_file, cutoff=5.51)

    hb_file = check(hc_file.split('.csv')[0].split('_matrix_hc')[0]+'_matrix_hb.csv')
    # creates the small csv file
    HBondInteraction(sel1, sel2, NFRAME, sel_output, ncores=0)
    # Merging the dataframe
    hb_df = MergingHBDataframe(sel_output, NFRAME, ncores=0)
    # cleaning df and creating the matrix
    HBMatrix(universe, nres, NFRAME, hb_df,hb_file=hb_file)

    print('*****************************')

    print('collecting raw results...')
    hcMatrix=np.loadtxt(hc_file)
    sbMatrix=np.loadtxt(sb_file)
    hbMatrix=np.loadtxt(hb_file)

    print('plotting raw results...')
    plot(hcMatrix,'hydrophobic contacs',short_name='hc')
    plot(sbMatrix,'salt bridges',short_name='sb')
    plot(sbMatrix,'hydrogen bonds',short_name='hb')

    print('*****************************')
    hcMatrix_filtered = filter(hcMatrix,'hydrophobic contacts interactions','hc')
    sbMatrix_filtered = filter(sbMatrix,'salt bridges interactions','sb')
    hbMatrix_filtered = filter(hbMatrix,'hydrogen bonds interactions','hb')
    print('*****************************')

    print('building ultimate interaction matrix')
    Matrix = hcMatrix_filtered+sbMatrix_filtered+hbMatrix_filtered

    print('plotting results...')
    plot(Matrix,'all interactions')

    print('writing filtered matrix to file...')
    filename = check(sel_output+"_matrix.csv")
    np.savetxt(filename,Matrix)
    print('>>>',filename)

print('*****************************')
print('done')
