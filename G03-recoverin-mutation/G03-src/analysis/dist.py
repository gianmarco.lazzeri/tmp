# dist.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i mdist.py <mol> -d <dcd> -i <input> -o <output> --start <start> --stop <stop> --step <step> -x -y -z --xy -f -c
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -x -> take the projection on the x axis
# -y -> take the projection on the y axis
# -z -> take the projection on the z axis
# --xy -> take the projection on the xy plane
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# --handle -> don't automatically save figures, do it by yourself
# EXAMPLE
# python -i mdist.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein --step 5
# TASK
# computes the distance between each couple of selections (center of mass)
# if just a single selection is specified: nothing happens
# plots results

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md distance analysis tool....')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
from MDAnalysis.analysis import align
from MDAnalysis.analysis.rms import rmsd

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='compute the distance between each couple of selections (center of mass)\nif specified, project on x,y,z axis, or xy plane\nif just a single selection is given: do nothing\nplot results')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='is a list of dcd or xtc files: coordinate time series\nif not specified: takes <molecule>.dcd as default')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
parser.add_argument("-s","--selection",nargs='+',help="is a list of 'selections': a subset of <mol> atoms\nneeds at least 2 elements")
parser.add_argument("-x","--x_axis",action='store_true',help='project everything on the x axis')
parser.add_argument("-y","--y_axis",action='store_true',help='project everything on the y axis')
parser.add_argument("-z","--z_axis",action='store_true',help='project everything on the z axis')
parser.add_argument("--xy",action='store_true',help='project everything on the xy plane')
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection
L = len(sel)
if L<=1: # at least 2 selections
    print('type at least two selections!')
else:

    print('*****************************')
    print('working directory:  ',folder)
    print('working on molecule:',mol)
    print('with data:          ','; '.join(dcd))
    print('selections:         ','; '.join(sel))
    print('output path:        ',output)
    print('loading data...')
    print('*****************************')

    # universe
    universe = mda.Universe(mol,dcd)

    # statistics
    print('   n frames:',universe.trajectory.n_frames)
    print('total atoms:',universe.select_atoms("all").n_atoms)
    print('    protein:',universe.select_atoms("protein or resname GLYM").n_atoms)
    print('   membrane:',universe.select_atoms("resname DGPS DGPC DGPE").n_atoms)
    print('    calcium:',universe.select_atoms("resname CAL").n_atoms)
    print('      water:',universe.select_atoms("resname TIP3").n_atoms)

    # start-step-stop
    start = max(0,args.start-1)
    step = args.step
    stop =(args.stop>=0)*min(args.stop+1,universe.trajectory.n_frames)+(args.stop<0)*universe.trajectory.n_frames
    I = [i for i in range(start,stop,step)]

    # MAIN CYCLE

    for l1 in range(L-1):

        sel_name_1 = sel[l1]

        # remove unnecessary spaces
        sel_name_1 = ' '.join([x for x in sel_name_1.split(' ') if x != ''])

        # first element of the couple
        real_sel_name_1 = sel_name_1
        # necessary with myrystoil groups
        if sel_name_1 == 'backbone':
            real_sel_name_1 = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
        if sel_name_1 == 'protein':
            real_sel_name_1 = "protein or same segid as protein"
        if sel_name_1 == 'membrane':
            real_sel_name_1 = "resname DGPS DGPC DGPE"

        selection_1 = universe.select_atoms(real_sel_name_1)
        

        sel_output_1 = output + '_' + '_'.join([x for x in sel_name_1.split(' ') if x not in mute_words])[0:N_sel]

        for l2 in range(l1+1,L):

            sel_name_2 = sel[l2]
            # remove unnecessary spaces
            sel_name_2 = ' '.join([x for x in sel_name_2.split(' ') if x != ''])
	    
            # second element of the couple
            real_sel_name_2 = sel_name_2
            # necessary with myrystoil groups
            if sel_name_2 == 'backbone':
                real_sel_name_2 = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
            if sel_name_2 == 'protein':
                real_sel_name_2 = "protein or same segid as protein"
            if sel_name_2 == 'membrane':
                real_sel_name_2 = "resname DGPS DGPC DGPE"



            selection_2 = universe.select_atoms(real_sel_name_2)

            print('*****************************')
            print('selection',sel_name_1,'vs selection',sel_name_2)
            print('selection 1 has',selection_1.n_atoms,'atoms')
            print('selection 2 has',selection_2.n_atoms,'atoms')
            print('preliminar alignment...',end='')

            align.AlignTraj(universe,universe,\
                            select=f'({real_sel_name_1}) or ({real_sel_name_2})',\
                            in_memory=True).run()
            print('done')

            sel_output_2 = '_VS' + '_' + '_'.join([x for x in sel_name_2.split(' ') if x not in mute_words])[0:N_sel]
  
            print(f'computing distance between selections for {len(I)} frames'+args.x_axis*'\n         (projecting on the x axis)'+args.y_axis*'\n         (projecting on the y axis)'+args.z_axis*'\n         (projecting on the z axis)'+args.xy*'\n         (projecting on the xy plane)')

            indices = range(3)
            if args.x_axis:
                indices = [0]
            if args.y_axis:
                indices = [1]
            if args.z_axis:
                indices = [2]
            if args.xy:
                indices = range(2)

            result = []
            #retrieve selection center of mass position
            i = 0
            for frame in universe.trajectory:
                if i in I:
                    result.append(np.linalg.norm(selection_1.center_of_mass()[indices]-selection_2.center_of_mass()[indices]))
                i += 1

            print('plotting results...')
            n_fig += 1
            plt.figure(n_fig)
            plt.grid()
            plt.title(f'distance between {" ".join([x for x in sel_name_1.split(" ") if x not in mute_words])} and {" ".join([x for x in sel_name_2.split(" ") if x not in mute_words])}\n(center of mass'+(args.x_axis)*', projection on the x axis'+(args.y_axis)*', projection on the y axis'+(args.z_axis)*', projection on the z axis'+(args.xy)*', projection on the xy plane'+')')
            plt.ylabel('distance')
            plt.xlabel('epocs')
            plt.xlim(min(I)-step,max(I)+step)
            plt.plot(I,result)
            if args.handle==False:
                filename = check(sel_output_1+sel_output_2+'_DIST.png')
                plt.savefig(filename)
                print('>>>',filename)
            print(f'>>> handle with plt.figure({n_fig})')

            print('saving results to file...')
            filename = check(filename.split('.png')[0]+'.csv')
            np.savetxt(filename,result)
            print('>>>',filename)
            print(f'distance is {np.mean(result):.3f} +- {np.std(result):.3f}  ({min(result):.3f}, {max(result):.3f})')
                
    print('*****************************')
    print('done')
