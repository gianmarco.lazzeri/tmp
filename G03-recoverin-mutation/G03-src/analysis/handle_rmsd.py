# log.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i rmsd.py <mol> -d <dcd> -i <input> -o <output> -r <reference> -s <selection> --start <start> --stop <stop> --step <step>
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -r <reference> is a list: can be either the reference frame in the (concatenated) <dcd> file 
# ...or a new pdb file to be taken as reference - default is '1'
# ...if 0 -> takes average structure of the <dcd>
# ...if -1 -> takes the last frame of the <dcd>
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> useless; here just to match gyr.py command line 
# EXAMPLE
# python -i rmsd.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein -r 0 -s "resname ALA" --step 5
# TASK
# for each element in <reference>, for each element in <selection>:
# ...retrieve "selection" rmsd with respect to reference and plot data (time series)
# ...retrieve contributes to rmsd according to residue and plot data (time series)

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
N_ref='1' # default frame for rmsd
sel_default = 'backbone' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files
base_cmap='nipy_spectral' # colors in plot

# BEGIN

print('*****************************')
print('md traject. replotting tool..')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# load file
flag = 1
def check(filename):
    global flag
    if os.path.isfile(filename)==False:
        if args.force:
            print(f'warning! {filename} does not exists')
            return 0
        flag = 0
        return(check(input(f'{filename} does not exists!\n  type filename: ')))
    if args.choose and flag:
        choice = input(f'suggested filename: {filename} - file exists\n  want to load? ')
        flag = 0
        if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
            flag = 1
            return filename
        return check(input('  type new filename: '))
    flag = 1
    return filename

def colors(N,base_cmap=base_cmap):
    if N>1:
        intervals = np.arange(0,1+1/(N-1),1/(N-1))
    else:
        intervals = [0.5]
    return [plt.get_cmap(base_cmap)(i) for i in intervals]

# PARSING

parser = argparse.ArgumentParser(description='for each element in <reference>, for each element in <selection>:\nretrieve "selection" rmsd with respect to reference and plot data (time series)\nretreive contributes to rmsd according to residue and plot data (time series)')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='useless; here just to match pca.py')
parser.add_argument("-i","--input",default='',help='input folder; just for retrieving file names')
parser.add_argument("-o","--output",default='',help='output path; just for retrieving file names')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"(just for plot titles)\nis a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
parser.add_argument("-r","--reference",default=[N_ref],nargs='+',help="(just for plot titles)\nis a list: can be either the reference frame in the (concatenated) <dcd> file \nor a new pdb file to be taken as reference; default is '1'\nif 0 takes the <dcd> average selection structure as reference\nif -1 takes the last frame of the <dcd>")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='useless; here just to match rmsd.py command line')
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# reference
ref = args.reference
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('as reference:       ','; '.join(ref))
print('output path:        ',output)
print('loading data...')

# start-step-stop
start = max(0,args.start-1)
step = args.step

# MAIN CYCLE

for ref_name in ref:
    #retrieve reference frame
    try:
        i = int(ref_name)
        if i:
            i = max(i-1,-1)
            ref_title = 'reference frame: '+ref_name*(i>=0)+'last frame'*(i<0)
        else:
            ref_title = 'reference: selection average structure'
    except:
        ref_title = 'reference: '+ref_name

    for sel_name in sel:

        # remove unnecessary spaces
        sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])
        sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)


        print('*****************************')
        print('loading rmsd for selection:',sel_name)

        filename = check(sel_output+'_rmsd.csv')

        if filename:
            result = np.loadtxt(filename)

            stop =len(result)
            I = [i for i in range(start,stop*step,step)]

            print('plotting results...')

            n_fig += 1
            plt.figure(n_fig)
            plt.grid()
            plt.plot(I,result)
            plt.xlim(min(I)-step,max(I)+step)
            plt.title(f'RMSD for {" ".join([x for x in sel_name.split(" ") if x not in mute_words])}, '+ref_title)
            plt.ylabel('rmsd')
            plt.xlabel('epocs')
            print(f'>>> handle with plt.figure({n_fig})')

            print('loading residue-wise contribution to rmsd')
            filename = check(filename.split('.csv')[0]+'_resid.csv')
            temp = np.loadtxt(filename)
            mean_var = temp[0,:]
            max_var = temp[1,:]


            print('loading residues dictionary')
            filename = check(filename.split('.csv')[0]+'.dic')
            with open(filename) as f:
                residues = eval(f.readlines()[0])

            lr = len(max_var)

            # retrieve residues ordered list
            residues_list = ['' for i in range(lr)]
            for residue in residues:
                for i in residues[residue]:
                    residues_list[i] = residue

            print('plotting results...')
            n_fig += 1
            plt.figure(n_fig)
            
            ax = plt.subplot(111)
            ax.grid(zorder=0)
            plt.title(f'RMSD for {" ".join([x for x in sel_name.split(" ") if x not in mute_words])}, mean contribute to variance')
            ax.set_ylabel('rmsd^2')
            if lr < 16:
                ax.set_xticks(np.arange(lr), residues_list, rotation=45)
            else:
                ax.set_xticks([i for i in range(0,lr,int(lr/10))])
            ax.set_xlabel('residue')
            plt.xlim(0,len(max_var))
            plt.ylim(0,max(max_var))

            ax.set_prop_cycle(color=colors(len(residues)))

            for residue in residues:
                ax.bar([i+1/2 for i in residues[residue]],[max_var[i] for i in residues[residue]],width=1,color='0.64',zorder=3)
                ax.bar([i+1/2 for i in residues[residue]],[mean_var[i] for i in residues[residue]],width=1,label=residue,zorder=4)

            ax.legend(loc='upper center',ncol=int(len(residues)/3))
            print(f'>>> handle with plt.figure({n_fig})')
            
print('*****************************')
print('done')
