# log.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i rmsd.py <mol> -d <dcd> -i <input> -o <output> -r <reference> -s <selection> --start <n> --stop <n> --step <n> -f -c --handle
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -r <reference> is a list: can be either the reference frame in the (concatenated) <dcd> file 
# ...or a new pdb file to be taken as reference - default is '1'
# ...if 0 -> takes average structure of the <dcd>
# ...if -1 -> takes the last frame of the <dcd>
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'backbone'
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# --handle -> don't automatically save figures, do it by yourself
# EXAMPLE
# python -i rmsd.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein -r 0 -s "resname ALA" --step 5
# TASK
# for each element in <reference>, for each element in <selection>
# ...compute "selection" rmsd with respect to reference (time series)
# ...compute contributes to rmsd according to residue (time series)

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
N_ref='1' # default frame for rmsd
sel_default = 'backbone' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files
base_cmap='nipy_spectral' # colors in plot

# BEGIN

print('*****************************')
print('md trajectories analysis tool')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
from MDAnalysis.analysis import align
from MDAnalysis.analysis.rms import rmsd

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

def colors(N,base_cmap=base_cmap):
    if N>1:
        intervals = np.arange(0,1+1/(N-1),1/(N-1))
    else:
        intervals = [0.5]
    return [plt.get_cmap(base_cmap)(i) for i in intervals]

# PARSING

parser = argparse.ArgumentParser(description='for each element in <reference>, for each element in <selection>:\ncompute "selection" rmsd with respect to reference (time series)\ncompute contributes to rmsd according to residue (time series)')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='is a list of dcd or xtc files: coordinate time series\nif not specified: takes <molecule>.dcd as default')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"is a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
parser.add_argument("-r","--reference",default=[N_ref],nargs='+',help="is a list: can be either the reference frame in the (concatenated) <dcd> file \nor a new pdb file to be taken as reference; default is '1'\nif 0 takes the <dcd> average selection structure as reference\nif -1 takes the last frame of the <dcd>")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# reference
ref = args.reference
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('as reference:       ','; '.join(ref))
print('output path:        ',output)
print('loading data...')
print('*****************************')

# universe
universe = mda.Universe(mol,dcd)

# statistics
print('   n frames:',universe.trajectory.n_frames)
print('total atoms:',universe.select_atoms("all").n_atoms)
print('    protein:',universe.select_atoms("protein or resname GLYM").n_atoms)
print('   membrane:',universe.select_atoms("resname DGPS DGPC DGPE").n_atoms)
print('    calcium:',universe.select_atoms("resname CAL").n_atoms)
print('      water:',universe.select_atoms("resname TIP3").n_atoms)

# start-step-stop
start = max(0,args.start-1)
step = args.step
stop =(args.stop>=0)*min(args.stop+1,universe.trajectory.n_frames)+(args.stop<0)*universe.trajectory.n_frames
I = [i for i in range(start,stop,step)]

# MAIN CYCLE

for ref_name in ref:
    #retrieve reference frame
    try:
        i = int(ref_name)
        ref_universe = universe.copy()
        if i:
            i = max(int(i)-1,-1)
            ref_universe.trajectory[i]
            ref_title = 'reference frame: '+ref_name*(i>=0)+'last frame'*(i<0)
        else:
            ref_title = 'reference: selection average structure'
    except:
        ref_universe = mda.Universe(ref_name)
        ref_universe.trajectory[0]
        ref_title = 'reference: '+ref_name

    for sel_name in sel:

        # remove unnecessary spaces
        sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])

        real_sel_name = sel_name
        # necessary with myrystoil groups
        if sel_name == 'backbone':
            real_sel_name = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
        if sel_name == 'protein':
            real_sel_name = "protein or same segid as protein"
        if sel_name == 'membrane':
            real_sel_name = "resname DGPS DGPC DGPE"

        sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)


        print('*****************************')
        print('running rmsd for selection:',sel_name)
        print('selection has',universe.select_atoms(real_sel_name).n_atoms,'atoms')
        print(ref_title)

        if ref_title == 'reference: selection average structure':
            print('computing avg. sel. structure as reference... ',end='')
            # align reference to selection
            align.AlignTraj(ref_universe,ref_universe,select=real_sel_name,in_memory=True).run()
            # compute mean selection structure
            positions = np.mean(np.array([universe.trajectory[i].positions for i in I]),axis=0)
            # assing
            ref_universe.atoms.positions = positions
            print('done')

        print('aligning trajectory to reference... ',end='')
        aligner = align.AlignTraj(universe,ref_universe,select=real_sel_name,weights='mass',in_memory=True).run()
        selection = universe.select_atoms(real_sel_name)
        ref_selection = ref_universe.select_atoms(real_sel_name)
        print('done')
        
        print(f'computing rmsd for {len(I)} frames')
        result = []
        result_resid = []
        #retrieve selection position
        i = 0
        for frame in universe.trajectory:
            if i in I:
                result_resid.append([]) 
                result.append(rmsd(selection.positions,ref_selection.positions))
                for res in selection.residues:
                    resid = res.resid
                    segid = res.segid
                    residue = selection.select_atoms(f'resid {resid} and segid {segid}')
                    ref_residue = ref_selection.select_atoms(f'resid {resid} and segid {segid}')
                    result_resid[-1].append(rmsd(residue.positions,ref_residue.positions)**2)
            i += 1

        print('plotting results...')
        n_fig += 1
        plt.figure(n_fig)
        plt.grid()
        plt.plot(I,result)
        plt.xlim(min(I)-step,max(I)+step)
        plt.title(f'RMSD for {" ".join([x for x in sel_name.split(" ") if x not in mute_words])}, '+ref_title)
        plt.ylabel('rmsd')
        plt.xlabel('epocs')
        filename = check(sel_output+'_rmsd.png')
        if args.handle==False:
            plt.savefig(filename)
            print('>>>',filename)
        print(f'>>> handle with plt.figure({n_fig})')
        
        print('saving results to file...')
        filename = check(filename.split('.png')[0]+'.csv')
        np.savetxt(filename,result)
        print('>>>',filename)

        print('residue-wise contribution to rmsd')
        result_resid = np.array(result_resid)
        
        print('plotting results...')
        mean_var = [np.mean(result_resid[:,i]) for i in range(len(selection.residues))]
        max_var = [np.max(result_resid[:,i]) for i in range(len(selection.residues))]
        n_fig += 1
        plt.figure(n_fig)
        
        ax = plt.subplot(111)
        ax.grid(zorder=0)
        plt.title(f'RMSD for {" ".join([x for x in sel_name.split(" ") if x not in mute_words])}, mean contribute to variance')
        ax.set_ylabel('rmsd^2')
        lr = len(selection.residues)
        if lr < 16:
            ax.set_xticks(np.arange(lr), selection.residues.resnames, rotation=45)
        else:
            ax.set_xticks([i for i in range(0,lr,int(lr/10))])
        ax.set_xlabel('residue')
        plt.xlim(0,len(selection.residues))
        plt.ylim(0,max(max_var))
        
        i = 0
        residues={}
        for residue in selection.residues:
            residues[residue.resname]=[]

        for residue in selection.residues:
            residues[residue.resname].append(i)
            i+=1

        ax.set_prop_cycle(color=colors(len(residues)))

        for residue in residues:
            ax.bar([i+1/2 for i in residues[residue]],[max_var[i] for i in residues[residue]],width=1,color='0.64',zorder=3)
            ax.bar([i+1/2 for i in residues[residue]],[mean_var[i] for i in residues[residue]],width=1,label=residue,zorder=4)

        ax.legend(loc='upper center',ncol=int(len(residues)/3))
        if args.handle==False:
            filename = check(sel_output+'_rmsd_resid.png')
            plt.savefig(filename)
        print('>>>',filename)
        print(f'>>> handle with plt.figure({n_fig})')

        print('saving results to file...')
        filename = check(filename.split('.png')[0]+'.csv')
        np.savetxt(filename,np.array([mean_var,max_var]))
        print('>>>',filename)
        print('saving residues dictionary to file...')
        filename = check(filename.split('.csv')[0]+'.dic')
        with open(filename,'w') as f:
            f.writelines(f'{residues}')
        print('>>>',filename)

        print(f'highest contribution comes from resid {selection.residues[np.argmax(np.array(mean_var))].resid}: {selection.residues[np.argmax(mean_var)].resname}')  
            
print('*****************************')
print('done')
