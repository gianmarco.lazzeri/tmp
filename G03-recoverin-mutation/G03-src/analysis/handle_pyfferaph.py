# pyfferaph.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i pyfferaph.py <mol> -d <dcd> -i <input> -o <output> --start <start> --stop <stop> --step <step> -f -c
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'all'
# --filter -> choose persistance threshold value by yourself
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --stop <n> is the frame you end with (compatibly with step)
# --handle -> don't automatically save figures, do it by yourself
# EXAMPLE
# python -i pyfferaph.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein --step 5
# TASK
# build interaction matrices 'à la pyinteraph' (hc, sb, hb)
# filter persistent contacts and build a whole matrix
# plot results
# save network to file

# PARAMETERS

N_sel = 16 # in writing output file
sel_default = 'all' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files
N_vals=100 # n of values to choose persistence from
hc_file=''
sb_file=''
hb_file=''

# BEGIN

print('*****************************')
print('md pyfferaph rebuilding tool..')
print('loading libraries...')

# LIBRARIES

import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.patches as patches
from scipy.spatial.distance import squareform
import seaborn as sns
from itertools import product
import inspect
import pandas as pd
import time
import os 
import glob
from collections import namedtuple
import networkx as nx
from scipy.optimize import curve_fit
from scipy.optimize import fsolve
from scipy.special import expit

print('please check selections in the pyfferaph.py file')

# AUXILIARY FUNCTIONS

n_fig = 0 # handle plots

def plot(xxMatrix,name,short_name=''):
    l = len(xxMatrix[0])
    global n_fig
    global filename
    filename = filename.split('.dic')[0].split('.csv')[0].split('.png')[0].split('_hc')[0].split('_sb')[0].split('_hb')[0].split('_matrix')[0].split('_persistence')[0]
    n_fig += 1
    plt.figure(n_fig)
    ax = plt.axes()
    if len(segids.keys()) > 1:
        for i,segid in enumerate(segids):
            for x in segids[segid]:
                if x==segids[segid][0]:
                    ax.add_patch(patches.Rectangle((x,0),1,l,color=plt.get_cmap('Set1')(i),alpha=0.2,lw=0,label=segid))
                else:
                    ax.add_patch(patches.Rectangle((x,0),1,l,color=plt.get_cmap('Set1')(i),alpha=0.2,lw=0))                 
                ax.add_patch(patches.Rectangle((0,x),l,1,color=plt.get_cmap('Set1')(i),alpha=0.2,lw=0))
        sns.heatmap(xxMatrix,square=True,cmap='binary')
        ax.set(xticks=[0,l],yticks=[0,l],xticklabels=[0,l],yticklabels=[0,l])
        pos = ax.get_position()
        pos.x0 += 0.1
        ax.set_position(pos)
        plt.title((sel_name!=sel_default)*f'{sel_name} '+name+' matrix')
        plt.xlabel('residues')
        plt.ylabel('residues')
        plt.legend(bbox_to_anchor=(-0.067,1))
    else:
        sns.heatmap(xxMatrix,square=True)
        ax.set(xticks=[0,l],yticks=[0,l],xticklabels=[0,l],yticklabels=[0,l])
        plt.title((sel_name!=sel_default)*f'{sel_name} '+name+' matrix')
        plt.xlabel('residues')
        plt.ylabel('residues')

    if args.handle==False:
        filename = check(filename+'_'*(len(short_name)>0)+short_name+'_matrix.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

def filter(xxMatrix,name,short_name):
    global n_fig
    global filename
    filename = filename.split('.dic')[0].split('.csv')[0].split('.png')[0].split('_hc')[0].split('_sb')[0].split('_hb')[0].split('_matrix')[0].split('_persistence')[0]
    print(filename)
    print('filtering '+name+' interactions...')
    edges_size = []
    maxclustsize = [] # size of the biggest cluster according to threshold values
    for val in vals:       
        boolmat = [i>val for i in [xxMatrix]]
        G=nx.Graph(boolmat[0])
        maxclustsize.append(len(max(nx.connected_components(G), key=len)))
        edges_size.append(len(G.edges))
    print('fitting optimal persistence threshold...')
    fit = curve_fit(sigmoid, vals, maxclustsize, p0=(0.4,50,50,1), maxfev = 1000000)
    flex = fit[0][0]
    if flex > 0:
        print(f"> found flex at: {flex:.3f}")
    else:
        print('found negative flex: looking for max gap in maxclustsize instead')
        flex = vals[np.argmax(-np.diff(maxclustsize))+1]
        print(f"> found critical value at: {flex:.3f}")
    # closest val
    print('plotting results...')
    n_fig += 1
    plt.figure(n_fig)
    plt.grid()
    plt.plot(vals,maxclustsize, '.',label='max clust size')
    plt.plot(vals,edges_size, dashes=[2,2],label='n of edges')
    plt.xlim((0, 1))
    plt.ylim((min(maxclustsize)*0.95,max(edges_size)*1.05))
    plt.xlabel("$p_{min}$")
    plt.ylabel("size")
    x = np.linspace(max(vals),min(vals))
    plt.plot(x,sigmoid(x,*fit[0]),label='fit')
    if args.filter:
        plt.draw()
        plt.pause(0.001)
        while True:
            try:
                flex = float(input(" choose your optimal threshold value: "))
                break
            except:
                pass
    plt.plot(flex,sigmoid(flex,*fit[0]),'o',label = f'threshold ({flex:.2f},{sigmoid(flex,*fit[0]):.2f})', color = 'red')
    i = 0
    while vals[i]<flex and i < len(vals)-2:
        i += 1
    best_val = vals[i]
    plt.title((sel_name!=sel_default)*f'{sel_name} '+short_name+f' interactions: filtered {edges_size[i]} contacts')
    plt.legend()
    if args.handle==False:
        filename = check(filename+'_persistence_'+short_name+'.png')
        plt.savefig(filename)
        print('>>>',filename)
    print(f'>>> handle with plt.figure({n_fig})')

    boolmat = [i>best_val for i in [xxMatrix]][0]
    print('writing filtered matrix to file...')
    filename = filename.split('.dic')[0].split('.csv')[0].split('.png')[0].split('_hc')[0].split('_sb')[0].split('_hb')[0].split('_matrix')[0].split('_persistence')[0]
    filename = check(filename+'_matrix_'+short_name+'_filter.csv')
    np.savetxt(filename,boolmat)
    print('>>>',filename)
    return boolmat

# auxiliary functions

# Parametric sigmoid function for fitting
def sigmoid(x, x0, k, m, n): 
    y = m / (1 + np.exp(k*(x-x0))) + n
    return y


# write file, avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# load file
flag2 = 1
def check2(filename):
    global flag
    if os.path.isfile(filename)==False:
        if args.force:
            print(f'warning! {filename} does not exists')
            return 0
        flag2 = 0
        return(check(input(f'{filename} does not exists!\n  type filename: ')))
    if args.choose and flag:
        choice = input(f'suggested filename: {filename} - file exists\n  want to load? ')
        flag2 = 0
        if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
            flag2 = 1
            return filename
        return check(input('  type new filename: '))
    flag2 = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description="build interaction matrices 'à la pyinteraph' (hc, sb, hb)\nfilter persistent contacts and build a whole matrix\nplot results and save network to file")
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='useless; here just to match pyfferaph.py')
parser.add_argument("-i","--input",default='',help='input folder; just for retrieving file names')
parser.add_argument("-o","--output",default='',help='output path; just for retrieving file names')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help=f"(just for plot titles)\nis a list of 'selections': a subset of <mol> atoms; default is '{sel_default}'")
parser.add_argument("--filter",action='store_true',help="choose persistance threshold value by yourself")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
parser.add_argument("--handle",action='store_true',help="don't save figures: do it by yourself")
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
output +='_pyfferaph'
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('output path:        ',output)
print('loading data...')

# MAIN PROGRAM

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])
    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    vals = np.arange(0, 1, 1/N_vals) # acceptable persistence threshold values

    print('loading segments dictionary...')
    filename = check2(sel_output+'.dic')
    print('>>>',filename)
    with open(filename,'r') as f:
        text =f.readlines()[0]
    segids=eval(text)

    print('*****************************')
    print('loading pyfferaph for selection:',sel_name)

    hc_file = check2(sel_output+'_matrix_hc.csv')
    if hc_file == 0:
        break
    sb_file = check2(hc_file.split('_matrix_hc')[0]+'_matrix_sb.csv')
    if sb_file == 0:
        break
    hb_file = check2(sb_file.split('_matrix_sb')[0]+'_matrix_hb.csv')
    if hb_file == 0:
        break

    print('collecting raw results...')
    hcMatrix=np.loadtxt(hc_file)
    sbMatrix=np.loadtxt(sb_file)
    hbMatrix=np.loadtxt(hb_file)

    print('plotting raw results...')
    plot(hcMatrix,'hydrophobic contacs',short_name='hc')
    plot(sbMatrix,'salt bridges',short_name='sb')
    plot(sbMatrix,'hydrogen bonds',short_name='hb')

    print('*****************************')
    hcMatrix_filtered = filter(hcMatrix,'hydrophobic contacts interactions','hc')
    sbMatrix_filtered = filter(sbMatrix,'salt bridges interactions','sb')
    hbMatrix_filtered = filter(hbMatrix,'hydrogen bonds interactions','hb')
    print('*****************************')

    print('building ultimate interaction matrix')
    Matrix = hcMatrix_filtered+sbMatrix_filtered+hbMatrix_filtered

    print('plotting results...')
    plot(Matrix,'all interactions')

    print('writing filtered matrix to file...')
    filename = check(sel_output+"_matrix.csv")
    np.savetxt(filename,Matrix)
    print('>>>',filename)

print('*****************************')
print('done')
