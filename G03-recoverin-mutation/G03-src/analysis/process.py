# process.py
#
# SYNTAX
# (conva activate QCB_project)
# python -i process.py <mol> -d <dcd> -f <input> -o <output> -r <reference> -s <selection> --start <start> --stop <stop> --step <step>
# <mol> is either a pdb, a psf or a gro file
# -d <dcd> is a list of dcd or xtc files: coordinate time series
# ...if not specified: takes <mol>.dcd as default
# -i <input> is the input folder
# -o <output> is the output folder + prefix
# -s <selection> is a list of "selections": a subset of <mol>'s atoms - default is 'all'
# -n <number> number of frames to be kept (from the beginning)
# -f -> force, no complain if overwriting
# -c -> choose each file name time by time, --force wins over --choose!
# --start <n> is the frame you start from
# --step <n> is the interval between two scanned frames
# --end <n> is the frame you end at (compatibly with step)
# EXAMPLE
# python -i process.py protein.psf -d protein.dcd -i 01-raw/01-external -o ../../02-analysis/protein --step 5
# TASK
# for each element in <selection>: takes "selection" atoms in <mol>, discards the other
# extract a <output><mol><selection>.pdb file
# cat dcd files in a <output><mol><selection>.dcd file

# PARAMETERS
N_sel=20 # max n of characters for selection in default filenames
sel_default = 'all' # default value for selection
mute_words = ['','resname','name','segid','resid','type','moltype','and','or','(',')','{','}','index'] #discard in writing output files

# BEGIN

print('*****************************')
print('md processing analysis tool..')
print('loading libraries...')

# LIBRARIES

import argparse
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import MDAnalysis as mda
import MDAnalysis.analysis.pca as pca

# AUXILIARY FUNCTIONS

# avoid unexpected file overwriting
flag = 1
def check(filename):
    global flag
    if args.force!=True:
        if os.path.isfile(filename) or (args.choose and flag):
            text = os.path.isfile(filename)*(f'{filename} already exists!\n  want to overwrite? ')
            text += flag*max(0,args.choose-os.path.isfile(filename))*(f'suggested file name: {filename}\n')
            text += max(0,args.choose-os.path.isfile(filename))*('  want to keep? ')
            choice =   input(text)
            flag = 0
            if choice == 'y' or choice == 'yes' or choice == 'Y' or choice == 'Yes' or choice == 'YES':
                flag = 1
                return filename
            return check(input('  type new filename: '))
    flag = 1
    return filename

# PARSING

parser = argparse.ArgumentParser(description='for each element in <selection>: takes "selection" atoms in <mol>, discards the other\nextract a <output><mol><selection>.pdb file\ncat dcd files in a <output><mol><selection>.dcd file')
# main arguments
parser.add_argument("molecule",help='is either a .pdb, a .psf or a .gro file')
parser.add_argument("-d","--dcd",default=None,nargs='+',help='is a list of dcd or xtc files: coordinate time series\nif not specified: takes <molecule>.dcd as default')
parser.add_argument("-i","--input",default='',help='input folder')
parser.add_argument("-o","--output",default='',help='output path')
parser.add_argument("-s","--selection",default=[sel_default],nargs='+',help="is a list of 'selections': a subset of <mol> atoms; default is 'all'")
# writing to memory
parser.add_argument("-f","--force",action='store_true',help='force execution: never complain about overwriting files')
parser.add_argument("-c","--choose",action='store_true',help='choose each file name time by time, --force wins over --choose')
# fully optional
parser.add_argument("--start",type=int,default=1,help='is the frame you start from')
parser.add_argument("--step",type=int,default=1,help='is the interval between two scanned frames')
parser.add_argument("--stop",type=int,default=-1,help='is the frame you end with (compatibly with <step>)')
#
args   = parser.parse_args()

# mol
mol = args.molecule
# dcd
if args.dcd == None:
  dcd = [mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]+'.dcd']
else:
  dcd = args.dcd
# folder
folder = args.input
os.chdir(folder)
# output
output = args.output
if output=='':
  output=mol.split('.psf')[0].split('.pdb')[0].split('.gro')[0]
# selection
sel = args.selection

print('*****************************')
print('working directory:  ',folder)
print('working on molecule:',mol)
print('with data:          ','; '.join(dcd))
print('selection:          ','; '.join(sel))
print('output path:        ',output)
print('loading data...')
print('*****************************')

# universe and backbone
universe = mda.Universe(mol,dcd)

# statistics
print('   n frames:',universe.trajectory.n_frames)
print('total atoms:',universe.select_atoms("all").n_atoms)
print('    protein:',universe.select_atoms("protein or resname GLYM").n_atoms)
print('   membrane:',universe.select_atoms("resname DGPS DGPC DGPE").n_atoms)
print('    calcium:',universe.select_atoms("resname CAL").n_atoms)
print('      water:',universe.select_atoms("resname TIP3").n_atoms)

# start-step-stop
start = max(0,args.start-1)
step = args.step
stop =(args.stop>=0)*min(args.stop+1,universe.trajectory.n_frames)+(args.stop<0)*universe.trajectory.n_frames
I = [i for i in range(start,stop,step)]

# MAIN CYCLE

for sel_name in sel:

    # remove unnecessary spaces
    sel_name = ' '.join([x for x in sel_name.split(' ') if x != ''])

    real_sel_name = sel_name
    # necessary with myrystoil groups
    if sel_name == 'backbone':
        real_sel_name = "(name N HN CA HA C O OT1 OT2 HT2) and same segid as (resname MET ALA LYS)"  
    if sel_name == 'protein':
        real_sel_name = "protein or same segid as protein"
    if sel_name == 'membrane':
        real_sel_name = "resname DGPS DGPC DGPE"
    selection = universe.select_atoms(real_sel_name)

    sel_output = output + ('_' + '_'.join([x for x in sel_name.split(' ') if x not in mute_words])[0:N_sel])*(sel_name != sel_default)

    print('*****************************')
    print('running procedure for selection:',sel_name)
    print('selection has',selection.n_atoms,'atoms')

    print('writing pdb...')
    filename = check(sel_output+'.pdb')
    selection.write(filename)
    print('>>>',filename)

    print(f'extracting {len(I)} frames: writing dcd...')
    i = 0
    filename = check(filename.split('.pdb')[0].split('.gro')[0]+'.dcd')
    with mda.Writer(filename, selection.n_atoms) as W:    
        for ts in universe.trajectory:
            if i in I:
                W.write(selection)
            i += 1
    print('>>>',filename)

print('*****************************')
print('done')
