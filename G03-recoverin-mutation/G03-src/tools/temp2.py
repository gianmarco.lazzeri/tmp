
    def gyr(self,compute=True,save=True,plot=True,**args):
        """
        Return the gyration radius of "self.universe_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute gyration radius.
            If True and system data are not loaded: ask wheter to load a .csv.
            If False: load a .csv.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        verbose : bool, optional
            Default is False.
        output : bool, optional
            If False: don't return anything. Default is True.

        Returns
        -------
        T,results : tuple
            "T" is the time vector, "results" is the gyration radius vector.
        """
        # parameters
        fname  = ''
        suffix = '_rmsd'
        # optional parameters
        output_name    = args.get('output_name')\
                      if args.get('output_name')\
                      else self.output_name
        selection_name = args.get('selection_name')\
                      if args.get('selection_name')\
                      else self.__log['selection_name']
        start   = args.get('start')  if args.get('start')\
                      else self.__log['start']
        step    = args.get('step')   if args.get('step')\
                      else self.__log['step']
        stop    = args.get('stop')   if args.get('stop')\
                      else self.__log['stop']
        n       = args.get('n')       if args.get('n')       else 0
        choose  = args.get('choose')  if args.get('choose')\
                                                 is not None else True
        force   = args.get('force')   if args.get('force')   else False
        verbose = args.get('verbose') if args.get('verbose') else False
        # getting T,results
        if compute: # computing
            if self.__backup:
                self.load(False,start=start,stop=stop,step=step,
                          selection_name=selection_name)
                if verbose:
                    print('computing gyration radius '+\
                    f'for selection "{selection_name}"')
                results = [self.universe_selection.radius_of_gyration()\
                for frame in tqdm(self.universe.trajectory,disable=not verbose)]
                dt = self.dt
                T  = [x*dt for x in range(len(results))]
                if save: # saving
                    if verbose:
                        print('saving data to file')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    if verbose:
                        print(f'>>> {fname}')
            elif not force: # can't compute
                choice = input('warning: no system loaded!'\
                            +'\nloading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                elif output:
                    return []
            else: # if force option: can't but exit
                if verbose:
                    print('warning: no system loaded!')
                if output:
                    return []
        if not compute: # loading
            if verbose:
                print('reading data from database')
            fname = check_load(f'{self.fname(output_name)}{suffix}.csv',\
                               choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            if verbose:
                print(f'>>> {fname}')
        if plot: # plotting
            title = args.get('title') if args.get('title') else\
            f'{self.name} {self.short(selection_name)}, radius of gyration'\
            if self.name else f'{self.input_name.split("/")[-1]} '+\
                 f'{self.short(selection_name)}, radius of gyration'
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{n*dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            plt.draw()
            plt.pause(0.01)
            if verbose:
                print(f'plotted gyration radius; handle with'+\
                      f'figure({gcf().number})')
            if save:
                if verbose:
                    print('saving plot to file')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                if verbose:
                    print(f'>>> {fname}')
        if output:
            return T,results
        
    def rmsd(self,compute=True,save=True,plot=True,**args):
        """
        Return the rmsd of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute rmsd.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        reference_name : str, optional
            .pdb or .gro file employed in comparisons. If not specified, the
            "input_name" universe itself is taken as reference.
        reference_frame : int, optional
            Frame of the reference universe employed in comparisions.
            Relevant only when "reference_name" is not specified, so that the
            "input_name" universe itself is taken as reference.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of points (x2) you want to average to produce a smooth line
            in the plot. If not specified: none.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,results,results_residues : tuple
            "T" is the time vector, "results" is the rmsd vector,
            "results_residues" is the "(residue_name,rmsd2 vector)"
            list of tuples.
        """
        # parameters
        fname  = ''
        suffix = '_rmsd'
        # optional parameters
        output_name    = args.get('output_name')\
                      if args.get('output_name')\
                      else self.output_name
        selection_name   = args.get('selection_name')\
                        if args.get('selection_name')\
                        else self.__log['selection_name']
        reference_name   = args.get('reference_name')\
                        if type(args.get('reference_name')) is str\
                        else self.__log['reference_name']
        reference_frame  = args.get('reference_frame')\
                        if args.get('reference_frame')\
                        else self.__log['reference_frame']
        start   = args.get('start')  if args.get('start')\
                      else self.__log['start']
        step    = args.get('step')   if args.get('step')\
                      else self.__log['step']
        stop    = args.get('stop')   if args.get('stop')\
                      else self.__log['stop']
        n       = args.get('n')       if args.get('n')       else 0
        choose  = args.get('choose')  if args.get('choose')\
                                                 is not None else True
        force   = args.get('force')   if args.get('force')   else False
        verbose = args.get('verbose') if args.get('verbose') else False
        show_max= args.get('show_max')if args.get('show_max')else False
        # getting T,results
        if compute: # computing
            if self.__backup:
                self.load(False,start=start,stop=stop,step=step,
                          selection_name=selection_name,
                          reference_name=reference_name,
                          reference_frame=reference_frame)
                print('aligning trajectory to reference... ',end='')
                real_selection_name = ALIAS[selection_name]\
                 if selection_name in ALIAS else selection_name
                align.AlignTraj(self.universe,self.reference,\
                select=real_selection_name,weights='mass',in_memory=True).run()
                print('done')
                if verbose:
                    print('computing rmsd for selection "{selection_name}"')
                results = []
                results_residues = [ [] for res in\
                          self.universe_selection.residues]
                residues = [res.resname for res in\
                          self.universe_selection.residues]
                for frame in tqdm(self.universe.trajectory,disable=not verbose):
                    results.append(rmsd(self.universe_selection.positions,\
                                       self.reference_selection.positions))
                    for i,res in enumerate(self.universe_selection.residues):
                        resid = res.resid
                        segid = res.segid
                        residue = self.universe_selection\
                        .select_atoms(f'resid {resid} and segid {segid}')
                        ref_residue = self.reference_selection\
                        .select_atoms(f'resid {resid} and segid {segid}')
                        results_residues[i].\
                        append(rmsd(residue.positions,ref_residue.positions)**2)
                dt = self.dt
                T  = [x*dt for x in range(len(results))]
                if save: # saving
                    if verbose:
                        print('saving rmsd data to file')
                    fname = check_save(f'{self.fname(output_name)}{suffix}.csv',
                                       choose=choose,force=force)
                    savetxt(fname,transpose((T,results)),delimiter=',')
                    if verbose:
                        print(f'>>> {fname}')
                    if verbose:
                        print('saving residues dictionary to file')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                      '_residues.dic',choose=choose,force=force)
                    savetxt(fname,residues,fmt='%s')
                    if verbose:
                        print(f'>>> {fname}')
                    if verbose:
                        print('saving rmsd2 by residue data to file')
                    fname = check_save(f'{self.fname(output_name)}{suffix}'+\
                                      '_residues.csv',choose=choose,force=force)
                    savetxt(fname,results_residues,delimiter=',')
                    if verbose:
                        print(f'>>> {fname}')
            elif not force: # can't compute
                choice = input('warning: no system loaded!'\
                            +'\nloading .csv data instead? ').lower()
                if choice == 'y' or choice == 'yes' or choice == '':
                    compute = False
                else:
                    return []
            else: # if force option: can't but exit
                if verbose:
                    print('warning: no system loaded!')
                return []
        if not compute: # loading
            if verbose:
                print('reading data from database')
            fname = check_load(f'{self.fname(output_name)}{suffix}.csv',\
                               choose=choose,force=force)
            T,results = loadtxt(fname,delimiter=',',unpack=True)
            dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            if verbose:
                print(f'>>> {fname}')
            if verbose:
                print('reading residues dictionary')
            fname = check_load(f'{self.fname(output_name)}{suffix}'
                                '_residues.dic',choose=choose,force=force)
            with open(fname) as f:
                residues = [res.replace('\n','') for res in f.readlines()]
            dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            if verbose:
                print(f'>>> {fname}')
            if verbose:
                print('reading rmsd2 by residue from database')
            fname = check_load(f'{self.fname(output_name)}{suffix}'
                                '_residues.csv',choose=choose,force=force)
            results_residues = loadtxt(fname,delimiter=',')
            dt = T[1]-T[0] if T[1]-T[0] else DEF_DT # time step info
            if verbose:
                print(f'>>> {fname}')
        if plot: # plotting
            title = args.get('title') if args.get('title') else\
                    f'{self.name} {self.short(selection_name)}, RMSD'\
                    if self.name else f'{self.input_name.split("/")[-1]} '+\
                    f'{self.short(selection_name)}, RMSD '+\
                    f'with respect to {reference_name}' if reference_name else\
            f'{self.input_name.split("/")[-1]} {self.short(selection_name)}'+\
            f', RMSD with respect to frame {reference_frame}'
            figure()
            plt.plot(T,results,'.') # main plot
            if n: # print smooth mean line
                X = T[n:-n]
                Y = [mean(results[i-n:i+n+1])for i in range(n,len(results)-n)]
                plt.plot(X,Y,'r',label=f'{n*dt:3.2f} ns average')
                plt.legend()
            plt.grid()
            plt.xlabel('time [ns]')
            plt.ylabel('gyr radius [A]')
            plt.title(title)
            plt.draw()
            plt.pause(0.01)
            if verbose:
                print(f'plotted rmsd; handle with figure({gcf().number})')
            if save:
                if verbose:
                    print('saving plot to file')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                if verbose:
                    print(f'>>> {fname}')
            title = title.split('RMSD')[0]+'RMSD$^2$ contribution by residue'
            mean_var = [mean(x) for x in results_residues]
            max_var  = [max(x) for x in results_residues]\
                       if show_max else mean_var # default none
            figure(figsize=(8.533,4.8))
            plt.grid(zorder=0)
            plt.xlim(0,len(residues))
            plt.ylim(0,max(max_var))
            residues_dictionary={}
            for i,residue in enumerate(residues):
                if residue in residues_dictionary:
                    residues_dictionary[residue].append(i)
                else:
                    residues_dictionary[residue] = [i]
            col = colors(len(residues_dictionary))
            for i,residue in enumerate(residues_dictionary):
                plt.bar([j+1/2 for j in residues_dictionary[residue]],\
                        [max_var[j] for j in residues_dictionary[residue]],\
                        width=1,color='0.64',zorder=3)
                plt.bar([j+1/2 for j in residues_dictionary[residue]],\
                        [mean_var[j] for j in residues_dictionary[residue]],\
                        width=1,color=col[i],zorder=4,label=residue)
            plt.legend(loc='upper center',\
                       ncol=int(len(residues_dictionary)/3),fontsize=9)        
            plt.title(title)
            plt.draw()
            plt.pause(0.01)
            if verbose:
                print('plotted rmsd2 by residue; '+\
                     f'handle with figure({gcf().number})')
            if save:
                if verbose:
                    print('saving plot to file')
                fname = fname[:-4]+'.pdf' if fname\
                        else f'{self.fname(output_name)}{suffix}_by_residue.pdf'
                fname = check_save(fname,choose=choose,force=force)
                savefig(fname)
                if verbose:
                    print(f'>>> {fname}')    
        return T,results,\
             [(residues[i],results_residues[i]) for i in range(len(residues))]
             
    def pca(self,compute=True,save=True,plot=True,**args):
        """
        Return the pca of "self.universe_selection" with respect to
        "self.reference_selection".
        
        Parameters
        ----------
        compute : bool, default is True
            If True and system data are loaded: compute pca.
            If True and system data are not loaded: ask wheter to load data.
            If False: load data.
        save : bool, default is True
            If True: save results and plots (if present) to file.
        plot : bool, default is True
            If True: plot results.
        output_name : str, optional
            Default path for output files (.csv and .pdf) to be saved to,
            just for this method. If not specified, takes "self.output_name"
            instead.
        selection_name : str, optional
            Default selection employed in analysis. If not specified, DEF_REF
            is taken.
        start, step, stop, selection_name : optional
            If they are not specified, those of "self" shall be used.
        n : int, optional
            Number of principal components to be shown in plots/saved to .csv.
            If not specified: default is 3.
        choose, force : bool, optional
            Passed to check_load, check_save functions. Default is True, False.
        title : str, optional
            Title of the plot. If not specified: build it authomatically by
            previous information. It could be useful to define "self.name".
        show_max : bool, optional, default False
            If True: show max contribution to variance by residue.
        verbose : bool, optional
            Default is False.

        Returns
        -------
        T,princip
        """
