# SYNTAX
# vmd -args path/to/visualize.tcl path/to/molecule
# WARNING!
# path/to/molecule must be from G03-recoverin-mutation folder
# NO .pdb extension must be indicated
# the script can be launched from ANY subfolder of G03-recoverin-mutation
# OUTPUT
# visualizes the simulation, with all the .dcd file in the folder

set path_to_molecule [lindex $argv 0] ; list

# go to the "G03-recoverin-mutation" directory
set directory [pwd] ; list
set directory_list [split $directory {/}] ; list
set main_directory "" ; list
foreach folder $directory_list {
	if {$folder == "G03-recoverin-mutation"} {
    append main_directory "G03-recoverin-mutation"
    break;
  }
	append main_directory $folder
	append main_directory "/"
}
cd $main_directory


# load the molecule in vmd
mol new $path_to_molecule.pdb
for {set i 0} {$i < 2} {incr i} {
  mol addfile [join [list $path_to_molecule "_simul_" $i ".dcd"] ""]
}

# return to old directory
cd $directory
