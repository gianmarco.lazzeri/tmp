#!/bin/bash
#PBS -l select=1:ncpus=8
#PBS -q short_cpuQ
#PBS -l walltime=00:05:00
#PBS -N rec_r implicit md simul
#PBS -o log.out
#PBS -e log.err
#PBS -M gianmarco.lazzeri@studenti.unitn.it
#PBS -m abe    # a-b-e means send email if the job is Aborted, Begins, Ends.

# parameters to be set

NAMD_EXE=$HOME/G03-src/external/NAMD_2.13_Linux-x86_64-multicore/namd2
NAMD_CONF=__set_me__
LOG_FILE=__set_me__

cd ${PBS_O_WORKDIR}
$NAMD_EXE +p8 +setcpuaffinity +devices 0 $NAMD_CONF > $LOG_FILE
