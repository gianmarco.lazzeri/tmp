FOR NON MUTATED PROTEIN

--> this part has problems <--

# create <mol>.psf and <mol>_mutated.psf
# create <mol>.pdb and <mol>_mutated.pdb
# warning! check if psf and pdb names of the atoms match!
vmd -args <mol> < generate.tcl

# create <mol>_mutated_solvated_ionized.pdb and psf
# create <mol>_water_free.fix <mol>_protein_free.fix <mol>_tails_free.fix templates
# warning! .fix files have to be updated with the results of simulations
#          (otherwise constraints shall keep atoms in the <mol>_solvated_ionized.pdb configuration)
vmd -args <mol_mutated> < membrane_solvate.tcl

--> start from here <-- solvated and ionized molecule

# system energy minimization
# create rec_r_em.pdb and psf
qsub non_mutated_membrane1.pbs

# rsync 05-membrane folder (cluster to pc)
# create rec_r_em.water_free.fix file
vmd -args rec_r_em < membrane_fix.tcl
# rsync 05-membrane folder (pc to cluster)

# water npt minimization and md
# create rec_r_water_eq.pdb and psf and dcd
qsub non_mutated_membrane2.pbs

# rsync 05-membrane folder (cluster to pc)
# create rec_r_water_eq.protein_free.fix file
vmd -args rec_r_water_eq < membrane_fix.tcl
# rsync 05-membrane folder (pc to cluster)

# protein nvt minimization ad md
# create rec_r_protein_eq.pdb and psf and dcd
qsub non_mutated_membrane3.pbs

# rsync 05-membrane folder (cluster to pc)
# create rec_r_protein_eq.tails_free.fix file
vmd -args rec_r_protein_eq < membrane_fix.tcl
# rsync 05-membrane folder (pc to cluster)

# tails nvt minimization and md
# create rec_r_tails_eq.pdb and psf and dcd
qsub non_mutated_membrane4.pbs

# nvt minimization and md
# create rec_r_nvt_eq.pdb and psf and dcd
qsub non_mutated_membrane5.pbs

# npt minimization and md
# create rec_r_md.pdb and psf and dcd
qsub non_mutated_membrane6.pbs

# restart npt for 5 ns md, 40 fold = 200 ns
# 100 frames each ns = 10000 frames
# create <mol>_mutated_npt_md.<2-41>.dcd
# warning! if the cluster is too slow, each job may not be able to run 10 ns md
#          in this case, further "qsub membrane.pbs" are necessary
#          namd.conf sets firsttimestep authomatically to that of last restart file
chmod 777 non_mutated_membrane_loop.sh
./non_mutated_membrane_loop.sh
