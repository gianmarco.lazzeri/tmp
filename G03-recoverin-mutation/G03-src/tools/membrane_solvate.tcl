# SYNTAX
# vmd -dispdev text -eofexit -args path/to/molecule < path/to/file.tcl
# WARNING!
# path/to/molecule must be from G03-recoverin-mutation folder
# NO .pdb extension must be indicated
# the script can be launched from ANY subfolder of G03-recoverin-mutation
# OUTPUT
# solvates and ionizes

# MAIN PARAMETERS
set top_directory "G03-src/external/charmm_toppar_namd"; list ; # topology directory

# go to the "G03-recoverin-mutation" directory
set directory [pwd] ; list
set directory_list [split $directory {/}] ; list
set main_directory "" ; list
foreach folder $directory_list {
	if {$folder == "G03-recoverin-mutation"} {
    append main_directory "G03-recoverin-mutation"
    break;
  }
	append main_directory $folder
	append main_directory "/"
}
cd $main_directory

# take path to molecule (pdb file)
set path_to_molecule [lindex $argv 0] ; list
set psf $path_to_molecule.psf
set pdb $path_to_molecule.pdb
set tmp $path_to_molecule.tmp
set out [join [list $path_to_molecule "_solvated_ionized"] ""] ; list
puts $out


# load the molecule in vmd
mol new $psf
mol addfile $pdb

# solvation box
set dx 90 ; list
set dy 90 ; list
set dz 150.	 ; list ; # z dimension
set box_min [list [expr -$dx/2] [expr -$dy/2] [expr -$dz/2]]
set box_max [list [expr +$dx/2] [expr +$dy/2] [expr +$dz/2]]
set size [list $box_min $box_max]

package require solvate
solvate $psf $pdb -o $tmp -minmax $size


# membrane size
set membrane [atomselect top "chain L"]  ; list ; # search for lipids
set membrane_size [measure minmax $membrane]  ; list
set padding 0
set min_z [expr [lindex $membrane_size 0 2] - $padding] ; list
set max_z [expr [lindex $membrane_size 1 2] + $padding]  ; list

# keep water far aside from membrane
set water [atomselect top "water and same residue as(z<$min_z or z>$max_z)"]
$water writepdb $tmp.water.pdb
$water writepsf $tmp.water.psf

mol delete all
package require psfgen
resetpsf
readpsf $psf
coordpdb $pdb
readpsf $tmp.water.psf
coordpdb $tmp.water.pdb
writepdb $tmp.pdb
writepsf $tmp.psf

# delete useless temporary file
file delete $tmp.water.psf
file delete $tmp.water.pdb

package require autoionize


autoionize -psf $tmp.psf -pdb $tmp.pdb -o $tmp -nions { { MG 1 } }
autoionize -psf $tmp.psf -pdb $tmp.pdb -o $out -sc 0.15 -cation POT

mol delete all
mol new $out.psf
mol addfile $out.pdb

# equilibrate water and ions (except calcium): create fix file
# WARNING! IT HAS TO BE MODIFIED AFTER PRELIMINARY SIMULATIONS
set seltext "protein or name CAL or (resname DGPC DGPE DGPS)"

set all [atomselect top "all"]
set fix [atomselect top $seltext]

$all set beta 0
$fix set beta 1

$all writepdb $out.water_free.fix

# equilibrate lipid tails: create fix file
# WARNING! IT HAS TO BE MODIFIED AFTER PRELIMINARY SIMULATIONS
set seltext {resname "DGP." and (name "[CO]2.*" or name "[CO]3.*" or name "H.*[RSXYTZ]")}
set selmyristoil {resname "GLYM" and (name "[COH][0123456789]+.")}

set all [atomselect top "all"]
set free [atomselect top $seltext]
set free_myristoil [atomselect top $selmyristoil]

$all set beta 1
$free set beta 0
$free_myristoil set beta 0

$all writepdb $out.tails_free.fix

# equilibrate protein: create fix file
# WARNING! IT HAS TO BE MODIFIED AFTER PRELIMINARY SIMULATIONS
set seltext "protein"

set all [atomselect top "all"]
set free [atomselect top $seltext]

$all set beta 1
$free set beta 0
$free_myristoil set beta 1

$all writepdb $out.protein_free.fix
