# SYNTAX
# vmd -dispdev text -eofexit -args path/to/molecule < path/to/file.tcl
# WARNING!
# path/to/molecule must be from G03-recoverin-mutation folder
# NO .pdb extension must be indicated
# the script can be launched from ANY subfolder of G03-recoverin-mutation
# OUTPUT
# solvates the molecule, puts it in a proper box, adds salt 
# saves the results in path/to/molecule_solvated, creates a psf

# MAIN PARAMETERS
set top_directory "G03-src/external/charmm_toppar_namd"; # topology directory
set padding   14 ; # (Angstroms) padding in box construction
set mol_ions 0.15 ; # molar concentration of water ions 
                      # (aside from those for charge neutralization)

# go to the "G03-recoverin-mutation" directory
set directory [pwd] ; list
set directory_list [split $directory {/}] ; list
set main_directory "" ; list
foreach folder $directory_list {
	if {$folder == "G03-recoverin-mutation"} {
    append main_directory "G03-recoverin-mutation"
    break;
  }
	append main_directory $folder
	append main_directory "/"
}
cd $main_directory

# take path to molecule (pdb file)
set path_to_molecule [lindex $argv 0] ; list
# set output directory
set output [join [list $path_to_molecule "_solvated"] ""] ; list

# load the molecule in vmd
mol new $path_to_molecule.pdb

# solvate
package require solvate ; list
solvate $path_to_molecule.psf $path_to_molecule.pdb -o $output -t $padding
# center the box
set all [atomselect top "all"]
$all moveby [vecinvert [measure center $all]]
# update dpb
$all writepdb $output.pdb
# measure box dimensions
set m [measure minmax $all] ; list
set min [lindex $m  0] ; list
set max [lindex $m  1] ; list
puts ""
puts "MOLECULE BOX DIMENSIONS"
puts [join [list "dx = " [expr [lindex $max 0]-[lindex $min 0]]] ""]
puts [join [list "dy = " [expr [lindex $max 1]-[lindex $min 1]]] ""]
puts [join [list "dz = " [expr [lindex $max 2]-[lindex $min 2]]] ""]
puts ""
# add ions
package require autoionize ; list
autoionize -psf $output.psf -pdb $output.pdb -o $output -sc $mol_ions ; list
# add a magnesium and two clorides
autoionize -psf $output.psf -pdb $output.pdb -o $output -nions {{MG 1} {CLA 2}}; list
