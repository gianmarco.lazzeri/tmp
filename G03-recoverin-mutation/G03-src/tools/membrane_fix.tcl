# SYNTAX
# vmd -dispdev text -eofexit -args path/to/molecule < path/to/file.tcl
# WARNING!
# NO .pdb extension must be indicated
# OUTPUT
# generates all required fix files from specific pdb file

# MAIN PARAMETERS
set top_directory "G03-src/external/charmm_toppar_namd"; list ; # topology directory

# take path to molecule (pdb file)
set path_to_molecule [lindex $argv 0] ; list
set psf $path_to_molecule.psf
set pdb $path_to_molecule.pdb
set tmp $path_to_molecule.tmp
set out $path_to_molecule


# load the molecule in vmd
mol new $pdb

# equilibrate water and ions (except calcium): create fix file
# WARNING! IT HAS TO BE MODIFIED AFTER PRELIMINARY SIMULATIONS
set seltext "protein or name CAL or (resname DGPC DGPE DGPS)"

set all [atomselect top "all"]
set fix [atomselect top $seltext]

$all set beta 0
$fix set beta 1

$all writepdb $out.water_free.fix

# equilibrate lipid tails: create fix file
# WARNING! IT HAS TO BE MODIFIED AFTER PRELIMINARY SIMULATIONS
set seltext {resname "DGP." and (name "[CO]2.*" or name "[CO]3.*" or name "H.*[RSXYTZ]")}
set selmyristoil {resname "GLYM" and (name "[COH][0123456789]+.")}

set all [atomselect top "all"]
set free [atomselect top $seltext]
set free_myristoil [atomselect top $selmyristoil]

$all set beta 1
$free set beta 0
$free_myristoil set beta 0

$all writepdb $out.tails_free.fix

# equilibrate protein: create fix file
# WARNING! IT HAS TO BE MODIFIED AFTER PRELIMINARY SIMULATIONS
set seltext "protein"

set all [atomselect top "all"]
set free [atomselect top $seltext]

$all set beta 1
$free set beta 0
$free_myristoil set beta 1

$all writepdb $out.protein_free.fix
