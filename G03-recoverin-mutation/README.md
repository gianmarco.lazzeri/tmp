Recoverin mutation
============================
This project aims at studying the following mutation of recoverin:  
**M132A**.

The starting configurations for recoverin with and without peptide can be found in 
folder `G03-data/00-external/recoverin`
